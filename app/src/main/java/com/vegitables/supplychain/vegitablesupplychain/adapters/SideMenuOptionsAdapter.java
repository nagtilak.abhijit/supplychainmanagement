package com.vegitables.supplychain.vegitablesupplychain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.models.SideMenuOption;

import java.util.List;

/**
 * Created by Abhi on 21-04-2019.
 */

public class SideMenuOptionsAdapter extends RecyclerView.Adapter<SideMenuOptionsAdapter.ViewHolder> {

    private Context mContext;
    private List<SideMenuOption> optionList;
    private ISidemenuListener listener;

    public SideMenuOptionsAdapter(Context mContext, List<SideMenuOption> optionList,
                                  ISidemenuListener listener) {
        this.mContext = mContext;
        this.optionList = optionList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.sidemenu_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

        viewHolder.optionImage.setImageResource(optionList.get(position).optionImage);
        viewHolder.optionName.setText(optionList.get(position).optionName);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.OnItemClick(position,optionList.get(position).optionName);
            }
        });

    }

    @Override
    public int getItemCount() {
        return optionList.size();
    }

    public interface ISidemenuListener {
        void OnItemClick(int position,String text);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView optionImage;
        TextView optionName;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            optionImage = (ImageView) itemView.findViewById(R.id.optionImage);
            optionName = (TextView) itemView.findViewById(R.id.optionName);
        }
    }
}

