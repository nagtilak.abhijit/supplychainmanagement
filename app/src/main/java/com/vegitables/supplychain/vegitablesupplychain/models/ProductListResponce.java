package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 22-04-2019.
 */

public class ProductListResponce {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("responseData")
    @Expose
    public ProductList responseData;
    @SerializedName("status")
    @Expose
    public Boolean status;
}
