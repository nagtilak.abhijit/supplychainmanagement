package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.PlaceOrder;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import static com.vegitables.supplychain.vegitablesupplychain.activities.MainActivity.cart_badge;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ITEMCOUNT;
import static com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager.showLongToast;

/**
 * Created by Abhi on 22-05-2019.
 */

public class PaymentMethod extends BaseActivity implements IProgressBar, IApisCallbacks {
    private TextView mHeader;
    private ImageView mBackButton;
    private CustomProgressDialog progressDialog;
    private LinearLayout llcod;
    private LinearLayout llnet;
    private LinearLayout lldebit;
    private LinearLayout llpaytm;
    private PlaceOrder data;
    private TextView paymentBy;
    private TextView tprice;
    private Button btnProcees;
    private String paymentmethod = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        if (getIntent().hasExtra("data"))
            data = (PlaceOrder) getIntent().getSerializableExtra("data");

        initView();
        initListener();
    }

    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        mBackButton = (ImageView) findViewById(R.id.backButton);
        llcod = (LinearLayout) findViewById(R.id.cod);
        llnet = (LinearLayout) findViewById(R.id.net);
        lldebit = (LinearLayout) findViewById(R.id.debit);
        llpaytm = (LinearLayout) findViewById(R.id.paytm);
        tprice = (TextView) findViewById(R.id.tprice);
        paymentBy = (TextView) findViewById(R.id.paymentBy);
        btnProcees = (Button) findViewById(R.id.btnProcees);
    }

    @Override
    protected void initListener() {
        mHeader.setText("Payment");
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tprice.setText("Total Amount " + getString(R.string.Rs) + " " + data.totalPrice + "");
        paymentBy.setText("Payment Method : None");
        llcod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBackground("COD");
                paymentBy.setText("Payment Method : COD");
                paymentmethod = "COD";
            }
        });
        lldebit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBackground("DEBIT");
                paymentBy.setText("Payment Method : Debit Card");
                paymentmethod = "DEBIT";
            }
        });

        llnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBackground("NET");
                paymentBy.setText("Payment Method : NetBanking");
                paymentmethod = "NET";
            }
        });

        llpaytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBackground("PAYTM");
                paymentBy.setText("Payment Method : PAYTM");
                paymentmethod = "PAYTM";
            }
        });

        initProgressBar();


        btnProcees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //        if (!TextUtils.isEmpty(value))
                if (paymentmethod.equalsIgnoreCase("")) {
                    ToastManager.showLongToast(PaymentMethod.this, "Please select payment method");
                    return;
                }

                if (NetworkManager.isNetworkAvailable(PaymentMethod.this)) {
                    showProgressBar();
                    APIService service = new APIService(PaymentMethod.this);
                    service.placeOrderAPI(data);
                } else {
                    ToastManager.showLongToast(PaymentMethod.this, "No Internet Connection");
                }

            }
        });
    }

    private void changeBackground(String cod) {
        llcod.setBackgroundColor(getResources().getColor(R.color.color_white));
        llnet.setBackgroundColor(getResources().getColor(R.color.color_white));
        lldebit.setBackgroundColor(getResources().getColor(R.color.color_white));
        llpaytm.setBackgroundColor(getResources().getColor(R.color.color_white));
        if (cod.equalsIgnoreCase("COD")) {
            llcod.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else if (cod.equalsIgnoreCase("NET")) {
            llnet.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
        if (cod.equalsIgnoreCase("DEBIT")) {
            lldebit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else if (cod.equalsIgnoreCase("PAYTM")) {
            llpaytm.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


        }
    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        showLongToast(this, errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onSuccess(Object response, String requestTag) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_sell_completed);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        Button btnOrder = (Button) dialog.findViewById(R.id.btn_ord);
        Button btnHome = (Button) dialog.findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PaymentMethod.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(PaymentMethod.this, HotelOrderHistoryActivity.class));
            }
        });
        SharedPreferencesManager.setSomeStringValue(this, ITEMCOUNT, "0");
        cart_badge.setText("0");
        dialog.show();
    }

}
