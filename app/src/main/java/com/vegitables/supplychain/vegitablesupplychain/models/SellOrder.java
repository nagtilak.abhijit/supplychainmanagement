package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 28-04-2019.
 */

 public class SellOrder {
    @SerializedName("createdOn")
    @Expose
    public String createdOn;
    @SerializedName("farmerDetails")
    @Expose
    public User farmerDetails;
    @SerializedName("isShipped")
    @Expose
    public Boolean isShipped;
    @SerializedName("product")
    @Expose
    public Product product;
    @SerializedName("product_image")
    @Expose
    public String productImage;
    @SerializedName("quantity")
    @Expose
    public Integer quantity;
    @SerializedName("sell_order_token")
    @Expose
    public String sellOrderToken;
    @SerializedName("shippingAddress")
    @Expose
    public Object shippingAddress;
    @SerializedName("totalPrice")
    @Expose
    public Integer totalPrice;
    @SerializedName("price")
    @Expose
    public Integer price;
}
