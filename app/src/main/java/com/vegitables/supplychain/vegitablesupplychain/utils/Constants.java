package com.vegitables.supplychain.vegitablesupplychain.utils;

/**
 * Created by Abhi on 18-04-2019.
 */

public class Constants {

    public static final String BASE_URL = "http://35.200.186.16/supplychain/";
    public static final String PROFILE_BASE_URL = "http://35.200.186.16";
    public static final String LOGIN_ENDPOINT = "login";
    public static final String PRODUCT_LIST = "product";
    public static final String PRODUCT_LIST_USER = "product/{userid}";
    public static final String ADDRESSLIST = "address";
    public static final String ADD_NEW_ADDRESS = "address";

    public static final String REGISTRATION = "user";
    public static final String GET_PROFILE = "user/{userid}";
    public static final String GET_CART = "cart/{userid}";
    public static final String CHANGE_QUANTITY = "cart/{userid}";
    public static final String SELL_PRODUCT = "sell";
    public static final String PLACE_ORDER = "order";
    public static final String ADD_CART = "cart";
    public static final String FARMER_ORDER = "sell";
    public static final String ISLOGGEDIN = "isLoggedIn";
    public static final String LOGGEDINTIME = "loggedInTime";
    public static final String LOGOUTTIME = "loggedOutTime";
    public static final String LOGINTOKEN = "login_token";
    public static final String ACCOUNTNO = "accountNumber";
    public static final String FULLNAME = "fullName";
    public static final String MOBILE = "mobile";
    public static final String PANNO = "panNumber";
    public static final String PHOTO = "photo";
    public static final String USERNAME = "username";
    public static final String USERTYPE = "userType";
    public static final String ITEMCOUNT = "cartItems";

}
