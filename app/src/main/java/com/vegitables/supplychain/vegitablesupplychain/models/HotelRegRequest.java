package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 23-04-2019.
 */

public class HotelRegRequest {
    @SerializedName("userType")
    @Expose
    public String userType;
    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("fullName")
    @Expose
    public String fullName;
    @SerializedName("accountNumber")
    @Expose
    public String accountNumber;
    @SerializedName("panNumber")
    @Expose
    public String panNumber;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("hotelName")
    @Expose
    public String hotelName;
    @SerializedName("gstnNumber")
    @Expose
    public String gstnNumber;
}
