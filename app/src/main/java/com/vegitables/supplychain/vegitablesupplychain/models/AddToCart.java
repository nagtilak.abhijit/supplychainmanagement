package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 29-04-2019.
 */

public class AddToCart {
    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("cartItem")
    @Expose
    public CartItem cartItem;
}
