package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 25-04-2019.
 */

public class AddAddress {
    @SerializedName("addressLine1")
    @Expose
    public String addressLine1;
    @SerializedName("addressLine2")
    @Expose
    public String addressLine2;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("district")
    @Expose
    public String district;
    @SerializedName("taluka")
    @Expose
    public String taluka;
    @SerializedName("village")
    @Expose
    public String village;
    @SerializedName("pincode")
    @Expose
    public String pincode;
}
