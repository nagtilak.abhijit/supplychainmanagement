package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.adapters.SideMenuOptionsAdapter;
import com.vegitables.supplychain.vegitablesupplychain.fragments.ProductList_Fragment;
import com.vegitables.supplychain.vegitablesupplychain.models.SideMenuOption;
import com.vegitables.supplychain.vegitablesupplychain.utils.Constants;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import java.util.ArrayList;
import java.util.List;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.FULLNAME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ISLOGGEDIN;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PHOTO;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PROFILE_BASE_URL;

public class MainActivity extends BaseActivity implements SideMenuOptionsAdapter.ISidemenuListener {
    public static TextView cart_badge;
    private ImageView mSideMenuOptions;
    private RecyclerView mRvSideMenu;
    private SideMenuOptionsAdapter optionsAdapter;
    private List<SideMenuOption> optionList;
    private TextView mSidemenuProfileName;
    private ImageView mSidemenuProfileImage;
    private boolean isLoggedIn = false;
    private String mProfilePhoto;
    private String mProfileName;
    private FrameLayout llbadge;
    private String userId;
    private String userType;
    private String cartCount = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userId = SharedPreferencesManager.getSomeStringValue(this, Constants.USERNAME);
        userType = SharedPreferencesManager.getSomeStringValue(this, Constants.USERTYPE);
        cartCount = SharedPreferencesManager.getSomeStringValue(this, Constants.ITEMCOUNT);
        isLoggedIn = SharedPreferencesManager.getSomeBooleanValue(this, ISLOGGEDIN);

        initView();
        initListener();
        prepareSideMenuOptions();
        replaceFragment(new ProductList_Fragment());

    }

    @Override
    protected void initView() {
        mSidemenuProfileImage = (ImageView) findViewById(R.id.sidemenuProfileImage);
        mSidemenuProfileName = (TextView) findViewById(R.id.sidemenuProfileName);
        cart_badge = (TextView) findViewById(R.id.cart_badge);
        mSideMenuOptions = (ImageView) findViewById(R.id.sidemenuOptions);
        mRvSideMenu = (RecyclerView) findViewById(R.id.rvSideMenu);
        llbadge = (FrameLayout) findViewById(R.id.llbadge);
        mProfilePhoto = SharedPreferencesManager.getSomeStringValue(MainActivity.this, PHOTO);
        mProfileName = SharedPreferencesManager.getSomeStringValue(MainActivity.this, FULLNAME);

    }

    @Override
    protected void initListener() {
        mSideMenuOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSidemenu();
            }
        });
        llbadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLoggedIn)
                    startActivity(new Intent(MainActivity.this, ActivityCart.class));
                else
                    ToastManager.showLongToast(MainActivity.this, "Please Login First To view Cart");
            }
        });
        mSidemenuProfileName.setText(mProfileName);
        setImage(PROFILE_BASE_URL + mProfilePhoto);
        if (userType.equalsIgnoreCase("farmer")) {
            llbadge.setVisibility(View.GONE);

        } else {
            llbadge.setVisibility(View.VISIBLE);
            cart_badge.setText(cartCount);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout.isDrawerOpen(Gravity.START)) {
            mDrawerLayout.closeDrawer(Gravity.START);
            return;
        }

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 1) {
            finish();

        } else {
            super.onBackPressed();
        }
    }

    private void setAdapters() {
        optionsAdapter = new SideMenuOptionsAdapter(this, optionList, this);
        mRvSideMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRvSideMenu.setAdapter(optionsAdapter);
    }

    @Override
    public void OnItemClick(int position, String text) {
        closeSidemenu();
        if (text.equalsIgnoreCase("login"))
            startActivity(new Intent(this, LoginActivity.class));
        else if (text.equalsIgnoreCase("Register"))
            startActivity(new Intent(this, UserTypeActivity.class));
        else if (text.equalsIgnoreCase("logout"))
            logOutMethod();
        else if (text.equalsIgnoreCase("My Products")) {
            if (userType.equalsIgnoreCase("farmer"))
                startActivity(new Intent(this, FarmerOrderHistoryActivity.class));
            else
                ToastManager.showLongToast(this, "In Progress");
        } else if (text.equalsIgnoreCase("My Profile"))
            startActivity(new Intent(this, MyProfileActivity.class));
        else if (text.equalsIgnoreCase("My Orders"))
            startActivity(new Intent(this, HotelOrderHistoryActivity.class));
        else if (text.equalsIgnoreCase("My address")) {
            Intent intent = new Intent(this, ActivityAddress.class);
            intent.putExtra("value", "");
            intent.putExtra("price", "");
            startActivity(intent);
        }
        else if (text.equalsIgnoreCase("About Us")) {
            Intent intent = new Intent(this, AboutUsActivity.class);
            startActivity(intent);
        }    else if (text.equalsIgnoreCase("FAQ")) {
            Intent intent = new Intent(this, FAQActivity.class);
            startActivity(intent);
        }
    }

    private void logOutMethod() {
        SharedPreferencesManager.clearAll(this);
        changeSideMenu();
        replaceFragment(new ProductList_Fragment());
        SharedPreferencesManager.setSomeStringValue(this, Constants.ITEMCOUNT, "0");

    }

    private void changeSideMenu() {
        isLoggedIn = SharedPreferencesManager.getSomeBooleanValue(MainActivity.this, ISLOGGEDIN);
        prepareSideMenuOptions();
        mSidemenuProfileName.setText("Profile Name");
        setImage("");
    }

    private void setImage(String s) {
        RequestOptions options = new RequestOptions();
        options.error(R.drawable.profile_default_icon).placeholder(R.drawable.no_image).diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(this)
                .load(s)
                .apply(options)
                .into(mSidemenuProfileImage);

    }


    // prepaire side menu method
    private void prepareSideMenuOptions() {
        optionList = new ArrayList<>();

        String[] sideMenu = getResources().getStringArray(R.array.sideMenuOptions);
        int[] sideMenuicon = {R.drawable.ic_login, R.drawable.ic_reg, R.drawable.profile_default_icon, R.drawable.ic_payment, R.drawable.ic_payment, R.drawable.ic_payment, R.drawable.ic_nearplace, R.drawable.ic_aboutus, R.drawable.ic_faq, R.drawable.ic_setting, R.drawable.ic_logout};
        for (int i = 0; i < sideMenu.length; i++) {
            if (isLoggedIn) {
                if (!sideMenu[i].equalsIgnoreCase("Login") && !sideMenu[i].equalsIgnoreCase("Register")) {
                    SideMenuOption sLogin = new SideMenuOption();
                    sLogin.optionImage = R.drawable.sidemenu_btn;
                    sLogin.optionName = sideMenu[i];
                    sLogin.optionImage = sideMenuicon[i];
                    optionList.add(sLogin);
                    if (userType.equalsIgnoreCase("hotel")) {
                        if (sideMenu[i].equalsIgnoreCase("My Products")) {
                            optionList.remove(sLogin);
                        }
                    } else if (userType.equalsIgnoreCase("farmer")) {
                        if (sideMenu[i].equalsIgnoreCase("My Orders")) {
                            optionList.remove(sLogin);
                        }
                        if (sideMenu[i].equalsIgnoreCase("My address")) {
                            optionList.remove(sLogin);
                        }
                    }
                    if (sideMenu[i].equalsIgnoreCase("My Orders") && sideMenu[i].equalsIgnoreCase("My address")) {
                        optionList.remove(sLogin);
                    }
                }

            } else {
                if (!sideMenu[i].equalsIgnoreCase("Logout") && !sideMenu[i].equalsIgnoreCase("My Profile") && !sideMenu[i].equalsIgnoreCase("My Products") && !sideMenu[i].equalsIgnoreCase("My Orders") && !sideMenu[i].equalsIgnoreCase("My address")) {
                    SideMenuOption sLogin = new SideMenuOption();
                    sLogin.optionImage = R.drawable.sidemenu_btn;
                    sLogin.optionName = sideMenu[i];
                    sLogin.optionImage = sideMenuicon[i];
                    optionList.add(sLogin);

                }

            }

        }
        setAdapters();
    }

    // sidemenu option clicked method
    public void openSidemenu() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (!drawer.isDrawerOpen(GravityCompat.START))
            drawer.openDrawer(GravityCompat.START);
    }

    // close sidemenu
    public void closeSidemenu() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
    }

    public void replaceFragment(Fragment fragment) {
        try {
            String backStateName = fragment.getClass().getName();
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

            if (!fragmentPopped) { //fragment not in back stack, create it.
                FragmentTransaction ft = manager.beginTransaction();
                ft.replace(R.id.fragment_container, fragment);
                ft.addToBackStack(backStateName);
                ft.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        cart_badge.setText(SharedPreferencesManager.getSomeStringValue(this, Constants.ITEMCOUNT));
        if (SharedPreferencesManager.getSomeStringValue(this, PHOTO).equalsIgnoreCase("")) {
            mSidemenuProfileName.setText("Profile Name");
            setImage("");
        } else {
            mSidemenuProfileName.setText(SharedPreferencesManager.getSomeStringValue(this, FULLNAME));
            setImage(PROFILE_BASE_URL + SharedPreferencesManager.getSomeStringValue(this, PHOTO));
        }


    }
}
