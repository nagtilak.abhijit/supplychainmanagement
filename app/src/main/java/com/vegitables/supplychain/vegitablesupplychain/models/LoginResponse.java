package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Abhi on 21-04-2019.
 */

public class LoginResponse implements Serializable {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("responseData")
    @Expose
    public ResponseData responseData;
    @SerializedName("status")
    @Expose
    public Boolean status;



}
