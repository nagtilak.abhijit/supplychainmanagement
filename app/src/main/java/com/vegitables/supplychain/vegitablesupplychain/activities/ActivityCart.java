package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.adapters.CarttListAdaptor;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.Cart;
import com.vegitables.supplychain.vegitablesupplychain.models.CartProduct;
import com.vegitables.supplychain.vegitablesupplychain.models.InDecQuan;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginResponse;
import com.vegitables.supplychain.vegitablesupplychain.models.ProductToken;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import java.util.List;

import static com.vegitables.supplychain.vegitablesupplychain.activities.MainActivity.cart_badge;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ITEMCOUNT;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERNAME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager.showLongToast;

/**
 * Created by Abhi on 24-04-2019.
 */

public class ActivityCart extends BaseActivity implements IProgressBar, IApisCallbacks, CarttListAdaptor.OnQuantityChanged {
    private TextView mHeader;
    private ImageView mBackButton;
    private RecyclerView rvCart;
    private Dialog progressDialog;
    private String userId;
    private TextView tvCartEmpty;
    private FrameLayout framelayout;
    private CarttListAdaptor carttListAdaptor;
    private List<CartProduct> cartItems;
    private TextView totalPrice;
    private LinearLayout llfooter;
    private Button btnCheckout;
    private Cart loginResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        initView();
        initListener();

        userId = SharedPreferencesManager.getSomeStringValue(this, USERNAME);
        getCart();
    }


    private void getCart() {
        if (NetworkManager.isNetworkAvailable(this)) {
            showProgressBar();
            APIService service = new APIService(this);

            service.getCartItems(userId);
        } else {
            ToastManager.showLongToast(this, "No Internet Connection");
        }
    }

    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        totalPrice = (TextView) findViewById(R.id.totalPrice);
        mBackButton = (ImageView) findViewById(R.id.backButton);
        rvCart = (RecyclerView) findViewById(R.id.rvCart);
        tvCartEmpty = (TextView) findViewById(R.id.tvCartEmpty);
        framelayout = (FrameLayout) findViewById(R.id.framelayout);
        llfooter = (LinearLayout) findViewById(R.id.llfooter);
        btnCheckout = (Button) findViewById(R.id.btnCheckout);

    }

    @Override
    protected void initListener() {
        mHeader.setText(getString(R.string.cart));
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityCart.this, ActivityAddress.class);
                intent.putExtra("value", "checkout");
                intent.putExtra("price", loginResponse.responseData.cart.totalPrice);
                startActivity(intent);
            }
        });
        initProgressBar();


    }

    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        if (requestTag.equalsIgnoreCase("CHANGE_QUANTITY")) {
            Cart loginResponse = (Cart) response;
            if(loginResponse.status)
            getCart();
        } else {
            if (response != null) {
                 loginResponse = (Cart) response;
                if (loginResponse.status) {
                    cartItems = loginResponse.responseData.cart.cartItems;
                    tvCartEmpty.setVisibility(View.GONE);
                    framelayout.setVisibility(View.VISIBLE);
                    setAdaptor();
                    SharedPreferencesManager.setSomeStringValue(this, ITEMCOUNT, cartItems.size() + "");
                    cart_badge.setText(cartItems.size() + "");
                    llfooter.setVisibility(View.VISIBLE);
                    totalPrice.setText(loginResponse.responseData.cart.totalPrice);

                } else {
                    llfooter.setVisibility(View.GONE);
                    tvCartEmpty.setVisibility(View.VISIBLE);
                    framelayout.setVisibility(View.GONE);
                }

            } else {
                tvCartEmpty.setVisibility(View.VISIBLE);
                framelayout.setVisibility(View.GONE);
                SharedPreferencesManager.setSomeStringValue(this, ITEMCOUNT, "0");
                cart_badge.setText("0");

            }
        }
    }

    private void setAdaptor() {
        carttListAdaptor = new CarttListAdaptor(this, cartItems, this);
        rvCart.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvCart.setAdapter(carttListAdaptor);
    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        ToastManager.showLongToast(this, errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void addQuantity(CarttListAdaptor.ViewHolder viewHolder, int position) {
        if (!NetworkManager.isNetworkAvailable(this)) {
            showLongToast(this, getString(R.string.no_internet));
            return;
        }
        showProgressBar();
        InDecQuan addToCart = new InDecQuan();
        addToCart.action = "INCREASE_QUANTITY";
        ProductToken productToken = new ProductToken();
        productToken.id = cartItems.get(position).id;
        addToCart.cartItem = productToken;
        callApi(addToCart);
    }

    @Override
    public void minusQuantity(CarttListAdaptor.ViewHolder viewHolder, int position) {
        if (!NetworkManager.isNetworkAvailable(this)) {
            showLongToast(this, getString(R.string.no_internet));
            return;
        }
        showProgressBar();
        InDecQuan addToCart = new InDecQuan();
        addToCart.action = "DECREASE_QUANTITY";
        ProductToken productToken = new ProductToken();
        productToken.id = cartItems.get(position).id;
        addToCart.cartItem = productToken;
        callApi(addToCart);
    }

    private void callApi(InDecQuan addToCart) {
        APIService service = new APIService(this);
        service.ChangeQuantity(userId, addToCart);
    }
}
