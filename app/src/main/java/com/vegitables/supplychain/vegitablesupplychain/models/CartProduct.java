package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Abhi on 12-05-2019.
 */

 public  class CartProduct implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("productDetails")
    @Expose
    public SellProduct productDetails;
    @SerializedName("quantity")
    @Expose
    public String quantity;
}
