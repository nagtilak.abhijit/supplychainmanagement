package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Abhi on 29-04-2019.
 */

public class SellProduct implements Serializable {
    @SerializedName("createdOn")
    @Expose
    public String createdOn;
    @SerializedName("farmerDetails")
    @Expose
    public User farmerDetails;
    @SerializedName("isShipped")
    @Expose
    public Boolean isShipped;
    @SerializedName("product")
    @Expose
    public Product product;
    @SerializedName("product_image")
    @Expose
    public String productImage;
    @SerializedName("quality")
    @Expose
    public String quality;
    @SerializedName("quantity")
    @Expose
    public Integer quantity;
    @SerializedName("id")
    @Expose
    public String sellOrderToken;
    @SerializedName("totalPrice")
    @Expose
    public Integer totalPrice;
    @SerializedName("price")
    @Expose
    public Integer price;
    @SerializedName("isInCart")
    @Expose
    public Boolean isInCart;
    @SerializedName("orderStatus")
    @Expose
    public String orderStatus;
}
