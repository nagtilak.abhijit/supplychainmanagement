package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhi on 24-04-2019.
 */

public class ShippingAddressResponce {

    @SerializedName("address")
    @Expose
    public List<ShippingAddress> shippingAddresses = new ArrayList<>();
}
