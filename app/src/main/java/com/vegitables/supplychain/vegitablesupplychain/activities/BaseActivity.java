package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.vegitables.supplychain.vegitablesupplychain.R;

/**
 * Created by Abhi on 21-04-2019.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected abstract void initView();

    protected abstract void initListener();

}
