package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ISLOGGEDIN;

/**
 * Created by Abhi on 22-04-2019.
 */

public class SplashActivity extends BaseActivity {
    private boolean isLoggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initView();
        initListener();


        isLoggedIn = SharedPreferencesManager.getSomeBooleanValue(SplashActivity.this, ISLOGGEDIN);
    }

    @Override
    protected void initView() {
        new Handler().postDelayed(new Runnable() {

			/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                if (isLoggedIn) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();

                } else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();

                }
                // close this activity
                finish();
            }
        }, 5000);
    }

    @Override
    protected void initListener() {

    }

}
