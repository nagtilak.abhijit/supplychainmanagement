package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Abhi on 22-04-2019.
 */

public class ProductList {
    @SerializedName("Products")
    @Expose
    public List<Product> products = null;
}
