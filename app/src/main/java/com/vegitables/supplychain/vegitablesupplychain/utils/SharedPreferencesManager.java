package com.vegitables.supplychain.vegitablesupplychain.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Abhi on 21-04-2019.
 */

public class SharedPreferencesManager  {

    private static final String APP_NAME = "SupplyChain";

    private SharedPreferencesManager() {
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
    }

    public static String getSomeStringValue(Context context, String key) {
        return getSharedPreferences(context).getString(key, "");
    }

    public static int getSomeIntegerValue(Context context, String key) {
        return getSharedPreferences(context).getInt(key, 0);
    }

    public static boolean getSomeBooleanValue(Context context, String key) {
        return getSharedPreferences(context).getBoolean(key, false);
    }

    public static void setSomeStringValue(Context context, String key, String newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, newValue);
        editor.commit();
    }

    public static void setSomeIntegerValue(Context context, String key, Integer newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(key, newValue);
        editor.commit();
    }

    public static void setSomeBooleanValue(Context context, String key, boolean newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(key, newValue);
        editor.commit();
    }

    public static void clearAll(Context context) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.commit();
    }
}
