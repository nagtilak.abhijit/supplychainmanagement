package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Abhi on 29-04-2019.
 */

public class SellProductData {
    @SerializedName("Products")
    @Expose
    public List<SellProduct> products = null;

}
