package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 22-04-2019.
 */

public  class Brand {

    @SerializedName("brandName")
    @Expose
    public String brandName;
}
