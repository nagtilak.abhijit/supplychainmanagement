package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 22-04-2019.
 */

public class Category {
    @SerializedName("categoryName")
    @Expose
    public String categoryName;
}
