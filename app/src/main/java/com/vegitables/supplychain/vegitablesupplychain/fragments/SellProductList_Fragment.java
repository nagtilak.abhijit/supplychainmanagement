package com.vegitables.supplychain.vegitablesupplychain.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.adapters.SellProductListAdaptor;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.AddToCart;
import com.vegitables.supplychain.vegitablesupplychain.models.CartItem;
import com.vegitables.supplychain.vegitablesupplychain.models.HotelSellProduct;
import com.vegitables.supplychain.vegitablesupplychain.models.SellProduct;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.Constants;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;

import static com.vegitables.supplychain.vegitablesupplychain.activities.MainActivity.cart_badge;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ADD_CART;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.BASE_URL;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.FARMER_ORDER;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERNAME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager.showLongToast;

/**
 * Created by Abhi on 21-04-2019.
 */

public class SellProductList_Fragment extends BaseFragment implements IApisCallbacks, IProgressBar, SellProductListAdaptor.CartAdd {
    public List<SellProduct> products = new ArrayList<>();
    private String id;
    private View mView;
    private RecyclerView mRvProduct;
    private CustomProgressDialog progressDialog;
    private String userId;
    private TextView noProduct;
    private SellProductListAdaptor sellproductListAdaptor;

    public SellProductList_Fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.sellfragment_product_list, container, false);
        init(mView);
        initListeners();
        id = getArguments().getString("id");
        userId = SharedPreferencesManager.getSomeStringValue(getActivity(), USERNAME);
        getProductData();
        return mView;


    }

    private void getProductData() {
        if (!NetworkManager.isNetworkAvailable(getActivity())) {
            showLongToast(getActivity(), getString(R.string.no_internet));
            return;
        }
        showProgressBar();
        APIService service = new APIService(this);
        HttpUrl url = HttpUrl.parse(BASE_URL + FARMER_ORDER + "?userId=" + userId + "&productId="+id);
        service.getHotelSellProduct(url + "");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init(View view) {

        mRvProduct = (RecyclerView) view.findViewById(R.id.rvProducts);
        noProduct = (TextView) view.findViewById(R.id.noProduct);


    }


    @Override
    public void initListeners() {
        initProgressBar();
    }


    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        if (requestTag.equalsIgnoreCase(ADD_CART)) {
            cart_badge.setText(Integer.parseInt(SharedPreferencesManager.getSomeStringValue(getActivity(), Constants.ITEMCOUNT)) + 1 + "");
            SharedPreferencesManager.setSomeStringValue(getActivity(), Constants.ITEMCOUNT, Integer.parseInt(SharedPreferencesManager.getSomeStringValue(getActivity(), Constants.ITEMCOUNT)) + 1 + "");
            ToastManager.showLongToast(getActivity(), "Product Added into cart successfully");
            getProductData();
        } else {
            HotelSellProduct hotelSellProduct = (HotelSellProduct) response;
            if (hotelSellProduct.responseData.products.isEmpty()) {
                noProduct.setVisibility(View.VISIBLE);
                mRvProduct.setVisibility(View.GONE);
            } else {
                noProduct.setVisibility(View.GONE);
                mRvProduct.setVisibility(View.VISIBLE);
                products.clear();
                products.addAll(hotelSellProduct.responseData.products);
                setAdaptor();

            }
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        ToastManager.showLongToast(getActivity(), errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void setAdaptor() {
        sellproductListAdaptor = new SellProductListAdaptor(getActivity(), products, this);
        mRvProduct.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRvProduct.setAdapter(sellproductListAdaptor);
    }

    @Override
    public void addToCart(int position) {
        if (!NetworkManager.isNetworkAvailable(getActivity())) {
            showLongToast(getActivity(), getString(R.string.no_internet));
            return;
        }
        showProgressBar();
        AddToCart addToCart = new AddToCart();
        addToCart.userId = userId;
        CartItem cartItem = new CartItem();
        cartItem.price = products.get(position).product.price.toString();
        cartItem.sellOrderToken = products.get(position).sellOrderToken;
        addToCart.cartItem = cartItem;
        APIService service = new APIService(this);
        service.addTocart(addToCart);
    }
}
