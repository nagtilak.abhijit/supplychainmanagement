package com.vegitables.supplychain.vegitablesupplychain.interfaces;

/**
 * Created by Abhi on 21-04-2019.
 */
public interface IApisCallbacks<T> {

    void onSuccess(T response, String requestTag);

    void onFailure(String errorMessage);

}

