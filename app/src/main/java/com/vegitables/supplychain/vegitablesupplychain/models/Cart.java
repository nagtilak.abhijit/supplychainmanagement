package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 12-05-2019.
 */

public class Cart {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("responseData")
    @Expose
    public CartData responseData;
    @SerializedName("status")
    @Expose
    public Boolean status;
}
