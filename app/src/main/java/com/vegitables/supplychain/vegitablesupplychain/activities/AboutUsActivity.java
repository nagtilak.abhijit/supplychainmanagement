package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;

/**
 * Created by Abhi on 30-05-2019.
 */

public class AboutUsActivity extends BaseActivity {
    private TextView mHeader;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        initView();
        initListener();
    }

    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        mBackButton = (ImageView) findViewById(R.id.backButton);

    }

    @Override
    protected void initListener() {
        mHeader.setText("About Us");
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}
