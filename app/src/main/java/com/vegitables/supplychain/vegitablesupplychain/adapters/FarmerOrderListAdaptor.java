package com.vegitables.supplychain.vegitablesupplychain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.models.SellOrder;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PROFILE_BASE_URL;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERTYPE;

/**
 * Created by Abhi on 21-04-2019.
 */

public class FarmerOrderListAdaptor extends RecyclerView.Adapter<FarmerOrderListAdaptor.ViewHolder> {

    private final String userType;
    List<SellOrder> mOrdertList;
    private Context mContext;

    public FarmerOrderListAdaptor(Context mContext, List<SellOrder> mOrdertList) {
        this.mContext = mContext;
        this.mOrdertList = mOrdertList;
        userType = SharedPreferencesManager.getSomeStringValue(mContext, USERTYPE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.order_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        SellOrder sellOrder = mOrdertList.get(position);
        viewHolder.tvName.setText(sellOrder.product.productName);
        RequestOptions options = new RequestOptions();
        options.error(R.drawable.no_image).placeholder(R.drawable.no_image).diskCacheStrategy(DiskCacheStrategy.ALL);
        String imgpath = PROFILE_BASE_URL  + sellOrder.productImage;
        Glide.with(mContext)
                .load(imgpath)
                .apply(options)
                .into(viewHolder.catImageImagview);
        if (sellOrder.product.quality.equalsIgnoreCase("1"))
            viewHolder.tvCatName.setText("Product Quality : Good");
        else if (sellOrder.product.quality.equalsIgnoreCase("2"))
            viewHolder.tvCatName.setText("Product Quality : Better");
        else if (sellOrder.product.quality.equalsIgnoreCase("3"))
            viewHolder.tvCatName.setText("Product Quality : Best");
        viewHolder.tvBrandName.setText(sellOrder.product.brandName);
        viewHolder.tvDate.setText(ConvertDate(sellOrder.createdOn));
        viewHolder.tvPrice.setText(sellOrder.price + " "+mContext.getString(R.string.Rs));
        viewHolder.tvQuantity.setText(sellOrder.quantity + "");
        viewHolder.tvTotal.setText("Total Amount = " + sellOrder.totalPrice + " "+mContext.getString(R.string.Rs) );
    }

    @Override
    public int getItemCount() {
        return mOrdertList.size();
    }

    public String ConvertDate(String dateString) {
        // convert seconds to milliseconds
        Date date = new java.util.Date(Long.parseLong(dateString));
// the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy hh:mm a");
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT+5:30"));
        String formattedDate = sdf.format(date);
        return formattedDate;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        private final ImageView catImageImagview;
        private final TextView tvName;
        private final TextView tvCatName;
        private final TextView tvBrandName;
        private final TextView tvDate;
        private final TextView tvQuantity;
        private final TextView tvPrice;
        private final TextView tvTotal;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            catImageImagview = (ImageView) itemView.findViewById(R.id.catImageImagview);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCatName = (TextView) itemView.findViewById(R.id.tvCatName);
            tvBrandName = (TextView) itemView.findViewById(R.id.tvBrandName);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            tvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
            tvTotal = (TextView) itemView.findViewById(R.id.tvTotal);

        }
    }
}

