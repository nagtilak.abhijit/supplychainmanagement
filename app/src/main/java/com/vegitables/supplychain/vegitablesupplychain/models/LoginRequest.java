package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Abhi on 21-04-2019.
 */

public class LoginRequest implements Serializable {
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("password")
    @Expose
    public String password;
}
