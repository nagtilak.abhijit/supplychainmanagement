package com.vegitables.supplychain.vegitablesupplychain.interfaces;

import com.vegitables.supplychain.vegitablesupplychain.models.AddToCart;
import com.vegitables.supplychain.vegitablesupplychain.models.AddressResponce;
import com.vegitables.supplychain.vegitablesupplychain.models.Cart;
import com.vegitables.supplychain.vegitablesupplychain.models.FarmerOrdersResponce;
import com.vegitables.supplychain.vegitablesupplychain.models.HotelSellProduct;
import com.vegitables.supplychain.vegitablesupplychain.models.InDecQuan;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginRequest;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginResponse;
import com.vegitables.supplychain.vegitablesupplychain.models.NewAddress;
import com.vegitables.supplychain.vegitablesupplychain.models.PlaceOrder;
import com.vegitables.supplychain.vegitablesupplychain.models.ProductListResponce;
import com.vegitables.supplychain.vegitablesupplychain.models.PurchaseOrders;
import com.vegitables.supplychain.vegitablesupplychain.models.Response;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ADD_CART;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ADD_NEW_ADDRESS;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.CHANGE_QUANTITY;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.GET_CART;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.GET_PROFILE;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.LOGIN_ENDPOINT;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PLACE_ORDER;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PRODUCT_LIST;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PRODUCT_LIST_USER;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.REGISTRATION;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.SELL_PRODUCT;

/**
 * Created by Abhi on 21-04-2019.
 */

public interface IAPICall {

    @POST(LOGIN_ENDPOINT)
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @GET(PRODUCT_LIST)
    Call<ProductListResponce> getProducts();

    @GET(PRODUCT_LIST_USER)
    Call<ProductListResponce> getProducts(@Path("userid") String userId);

    @Multipart
    @POST(REGISTRATION)
    Call<LoginResponse> farmerRegistration(@Part MultipartBody.Part profile_pic,
                                           @Part("userId") RequestBody userId,
                                           @Part("password") RequestBody password,
                                           @Part("userType") RequestBody userType,
                                           @Part("fullName") RequestBody fullName,
                                           @Part("mobile") RequestBody mobile,
                                           @Part("panNumber") RequestBody panNumber,
                                           @Part("accountNumber") RequestBody accountNumber,
                                           @Part("firmName") RequestBody firm,
                                           @Part("ifscCode") RequestBody ifsc
    );

    @Multipart
    @POST(REGISTRATION)
    Call<LoginResponse> hotelRegistration(@Part MultipartBody.Part profile_pic,
                                          @Part("userId") RequestBody userId,
                                          @Part("password") RequestBody password,
                                          @Part("userType") RequestBody userType,
                                          @Part("fullName") RequestBody fullName,
                                          @Part("mobile") RequestBody mobile,
                                          @Part("panNumber") RequestBody panNumber,
                                          @Part("accountNumber") RequestBody accountNumber,
                                          @Part("hotelName") RequestBody hotelname,
                                          @Part("gstnNumber") RequestBody gstNo,
                                          @Part("ifscCode") RequestBody ifsc
    );

    @GET
    Call<AddressResponce> getAddressList(@Url String userId);

    @POST(ADD_NEW_ADDRESS)
    Call<Response> addNewAddress(@Body NewAddress shippingAddress);

    @GET(GET_PROFILE)
    Call<LoginResponse> getUserProfile(@Path("userid") String userId);

    @GET(GET_CART)
    Call<Cart> getCartItems(@Path("userid") String userId);

    @Multipart
    @POST(SELL_PRODUCT)
    Call<ProductListResponce> sellProduct(
            @Part MultipartBody.Part profile_pic,
            @Part("userId") RequestBody userIdReq,
            @Part("productId") RequestBody productIdReq,
            @Part("quantity") RequestBody productQuanReq,
            @Part("price") RequestBody productTotalPriceReq,

            @Part("quality") RequestBody productqualityReq
    );

    @GET
    Call<FarmerOrdersResponce> farmerOrders(@Url String userId);

    @Multipart
    @PUT(GET_PROFILE)
    Call<LoginResponse> farmerUpdate(@Path("userid") String userId,
                                     @Part MultipartBody.Part profile_pic,
                                     @Part("fullName") RequestBody fullName,
                                     @Part("mobile") RequestBody mobile,
                                     @Part("panNumber") RequestBody panNumber,
                                     @Part("accountNumber") RequestBody accountNumber,
                                     @Part("firmName") RequestBody ifsc,
                                     @Part("ifscCode") RequestBody firm);

    @Multipart
    @PUT(GET_PROFILE)
    Call<LoginResponse> hotelUpdate(@Path("userid") String userId,
                                    @Part MultipartBody.Part profile_pic,
                                    @Part("fullName") RequestBody fullName,
                                    @Part("mobile") RequestBody mobile,
                                    @Part("panNumber") RequestBody panNumber,
                                    @Part("accountNumber") RequestBody accountNumber,
                                    @Part("hotelName") RequestBody hotelname,
                                    @Part("gstnNumber") RequestBody gstNo,
                                    @Part("ifscCode") RequestBody ifsc);

    @GET
    Call<HotelSellProduct> HotelSellProduct(@Url String userId);

    @POST(ADD_CART)
    Call<LoginResponse> addCart(@Body AddToCart addToCart);

    @PUT(CHANGE_QUANTITY)
    Call<Cart> ChangeQuantity(@Path("userid") String userId, @Body InDecQuan addToCart);

    @POST(PLACE_ORDER)
    Call<LoginResponse> PlaceOrder(@Body PlaceOrder placeOrder);


    @GET
    Call<PurchaseOrders> hotelorder(@Url String userId);

}
