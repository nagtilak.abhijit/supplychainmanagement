package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Abhi on 14-05-2019.
 */

public  class OrderHotel {
    @SerializedName("PurchaseOrders")
    @Expose
    public List<OrderDetailHotel> purchaseOrders = null;
}
