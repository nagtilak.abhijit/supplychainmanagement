package com.vegitables.supplychain.vegitablesupplychain.interfaces;

/**
 * Created by Abhi on 21-04-2019.
 */

public interface IProgressBar {

    void initProgressBar();

    void showProgressBar();

    void hideProgressBar();

}

