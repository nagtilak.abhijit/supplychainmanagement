package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginResponse;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.Constants;
import com.vegitables.supplychain.vegitablesupplychain.utils.KeyboardManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.PermissionUtility;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.GET_PROFILE;
import static com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager.showLongToast;

/**
 * Created by Abhi on 21-04-2019.
 */

public class MyProfileActivity extends BaseActivity implements IProgressBar, IApisCallbacks {
    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;
    private TextView mHeader;
    private TextView mHeaderEdit;
    private Bitmap bitmap = null;
    private ImageView mBackButton;
    private String userId;
    private EditText fullnameEditText;
    private EditText usernameEditText;
    private EditText phoneNoEditText;
    private EditText panNoEditText;
    private EditText accountNoEditText;
    private EditText profilePhotoEditText;
    private CustomProgressDialog progressDialog;
    private EditText hotelNameEditText;
    private EditText gstEditText;
    private LinearLayout llhotelname;
    private LinearLayout llgst;
    private View view1;
    private ImageView userImageImagview;
    private Button btnEdit;
    private ImageView mEditImagview;
    private String userType;
    private EditText ifscEditText;
    private EditText firmEditText;
    private LinearLayout llfirm;

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);
        userId = SharedPreferencesManager.getSomeStringValue(this, Constants.USERNAME);
        userType = SharedPreferencesManager.getSomeStringValue(this, Constants.USERTYPE);
        initView();
        initListener();
        getProfileData();
    }

    private void getProfileData() {
        showProgressBar();
        APIService service = new APIService(this);
        service.getUserData(userId);
    }

    @Override
    protected void initView() {
        userImageImagview = (ImageView) findViewById(R.id.userImageImagview);
        mEditImagview = (ImageView) findViewById(R.id.editImagview);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        mHeader = (TextView) findViewById(R.id.headerText);
        mHeaderEdit = (TextView) findViewById(R.id.tvEdit);
        fullnameEditText = (EditText) findViewById(R.id.fullnameEditText);
        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        ifscEditText = (EditText) findViewById(R.id.ifscEditText);
        firmEditText = (EditText) findViewById(R.id.firmEditText);

        phoneNoEditText = (EditText) findViewById(R.id.phoneNoEditText);
        accountNoEditText = (EditText) findViewById(R.id.accountNoEditText);
        panNoEditText = (EditText) findViewById(R.id.panNoEditText);
        hotelNameEditText = (EditText) findViewById(R.id.hotelNameEditText);
        gstEditText = (EditText) findViewById(R.id.gstEditText);
        llhotelname = (LinearLayout) findViewById(R.id.llhotelname);
        llgst = (LinearLayout) findViewById(R.id.llgst);
        llfirm = (LinearLayout) findViewById(R.id.llfirm);
        view1 = (View) findViewById(R.id.view1);
        mBackButton = (ImageView) findViewById(R.id.backButton);


        enableDisableComponents(false);
    }

    private void enableDisableComponents(boolean value) {
        fullnameEditText.setEnabled(value);
        phoneNoEditText.setEnabled(value);
        accountNoEditText.setEnabled(value);
        panNoEditText.setEnabled(value);
        hotelNameEditText.setEnabled(value);
        gstEditText.setEnabled(value);
        if (value) {
            mEditImagview.setVisibility(View.VISIBLE);
            btnEdit.setVisibility(View.VISIBLE);
        } else {
            btnEdit.setVisibility(View.GONE);
            mEditImagview.setVisibility(View.GONE);
        }

    }

    @Override
    protected void initListener() {
        mHeaderEdit.setVisibility(View.VISIBLE);
        mHeader.setText("My Profile");
        if (userType.equalsIgnoreCase("farmer")) {
            llgst.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            llhotelname.setVisibility(View.GONE);
        } else if (userType.equalsIgnoreCase("hotel")) {
            llgst.setVisibility(View.VISIBLE);
            llfirm.setVisibility(View.GONE);
            llhotelname.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
        }
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mEditImagview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userImageImagviewClicked();
            }
        });
        mHeaderEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableDisableComponents(true);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userType.equalsIgnoreCase("farmer")) {
                    KeyboardManager.hideKeyboard(MyProfileActivity.this);
                    if (!NetworkManager.isNetworkAvailable(MyProfileActivity.this)) {
                        showLongToast(MyProfileActivity.this, getString(R.string.no_internet));
                        return;
                    }
                    MultipartBody.Part profile_pic = null;

                    if (bitmap != null) {

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
                        String currentTimeStamp = dateFormat.format(new Date());
                        File file = new File(getCacheDir(), currentTimeStamp + ".jpeg");
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                        byte[] bitmapdata = bos.toByteArray();

                        try {
                            file.createNewFile();
                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(bitmapdata);
                            fos.flush();
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), file);
                        profile_pic = MultipartBody.Part.createFormData("file", file.getName(), reqFile1);

                    }
                    showProgressBar();


                    RequestBody userFullNameReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), fullnameEditText.getText().toString());
                    RequestBody userPANReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), panNoEditText.getText().toString());
                    RequestBody userAccNoReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), accountNoEditText.getText().toString());
                    RequestBody userphoneReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), phoneNoEditText.getText().toString());

                    RequestBody userfirmpReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), firmEditText.getText().toString());
                    RequestBody userifscpReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), phoneNoEditText.getText().toString());

                    APIService service = new APIService(MyProfileActivity.this);
                    service.farmerUpdateProfile(userId, profile_pic, userFullNameReq, userphoneReq, userPANReq, userAccNoReq, userfirmpReq, userifscpReq);

                } else if (userType.equalsIgnoreCase("hotel")) {
                    KeyboardManager.hideKeyboard(MyProfileActivity.this);
                    if (!NetworkManager.isNetworkAvailable(MyProfileActivity.this)) {
                        showLongToast(MyProfileActivity.this, getString(R.string.no_internet));
                        return;
                    }

                    MultipartBody.Part profile_pic = null;

                    if (bitmap != null) {

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
                        String currentTimeStamp = dateFormat.format(new Date());
                        File file = new File(getCacheDir(), currentTimeStamp + ".jpeg");
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                        byte[] bitmapdata = bos.toByteArray();

                        try {
                            file.createNewFile();
                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(bitmapdata);
                            fos.flush();
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), file);
                        profile_pic = MultipartBody.Part.createFormData("file", file.getName(), reqFile1);

                    }
                    showProgressBar();

                    RequestBody userFullNameReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), fullnameEditText.getText().toString());
                    RequestBody userPANReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), panNoEditText.getText().toString());
                    RequestBody userAccNoReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), accountNoEditText.getText().toString());
                    RequestBody userphoneReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), phoneNoEditText.getText().toString());

                    RequestBody userHotelNameReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), hotelNameEditText.getText().toString());
                    RequestBody usergstReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), gstEditText.getText().toString());
                    RequestBody userifscReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), ifscEditText.getText().toString());

                    APIService service = new APIService(MyProfileActivity.this);
                    service.hotelUpdateProfile(userId, profile_pic, userFullNameReq, userphoneReq, userPANReq, userAccNoReq, userHotelNameReq, usergstReq, userifscReq);

                }
            }
        });
        initProgressBar();
    }

    private void userImageImagviewClicked() {
        selectImage();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    boolean result = PermissionUtility.checkCameraPermission(MyProfileActivity.this);
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    boolean result = PermissionUtility.checkStoragePermission(MyProfileActivity.this);
                    if (result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    // camera intent
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    // gallery intent
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        if (requestTag.equalsIgnoreCase(GET_PROFILE)) {
            LoginResponse loginResponse = (LoginResponse) response;
            showLongToast(this, loginResponse.message);
            updateValues(loginResponse);
        } else if (requestTag.equalsIgnoreCase("farmer Update")) {
            LoginResponse loginResponse = (LoginResponse) response;
            showLongToast(this, loginResponse.status + "");
            enableDisableComponents(false);

        } else if (requestTag.equalsIgnoreCase("hotel Update")) {
            LoginResponse loginResponse = (LoginResponse) response;
            showLongToast(this, loginResponse.status + "");
            enableDisableComponents(false);

        }
    }

    private void updateValues(LoginResponse loginResponse) {
        if (userType.equalsIgnoreCase("farmer")) {
            RequestOptions options = new RequestOptions();
            options.error(R.drawable.profile_default_icon).placeholder(R.drawable.no_image).diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(this)
                    .load(Constants.PROFILE_BASE_URL + loginResponse.responseData.user.photo)
                    .apply(options)
                    .into(userImageImagview);
            fullnameEditText.setText(loginResponse.responseData.user.fullName);
            phoneNoEditText.setText(loginResponse.responseData.user.mobile);
            usernameEditText.setText(loginResponse.responseData.user.username);
            panNoEditText.setText(loginResponse.responseData.user.panNumber);
            accountNoEditText.setText(loginResponse.responseData.user.accountNumber);
            ifscEditText.setText(loginResponse.responseData.user.ifscCode);
            firmEditText.setText(loginResponse.responseData.firmName);
        } else {
            RequestOptions options = new RequestOptions();
            options.error(R.drawable.profile_default_icon).placeholder(R.drawable.no_image).diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(this)
                    .load(Constants.PROFILE_BASE_URL + loginResponse.responseData.user.photo)
                    .apply(options)
                    .into(userImageImagview);
            fullnameEditText.setText(loginResponse.responseData.user.fullName);
            phoneNoEditText.setText(loginResponse.responseData.user.mobile);
            usernameEditText.setText(loginResponse.responseData.user.username);
            panNoEditText.setText(loginResponse.responseData.user.panNumber);
            accountNoEditText.setText(loginResponse.responseData.user.accountNumber);
            gstEditText.setText(loginResponse.responseData.gstnNumber);
            hotelNameEditText.setText(loginResponse.responseData.hotelName);
            ifscEditText.setText(loginResponse.responseData.user.ifscCode);
        }

    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        showLongToast(this, errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);

        }
    }

    // select image from gallery
    private void onSelectFromGalleryResult(Intent data) {

        Uri imageUri = data.getData();

        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            userImageImagview.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // capture image from camra
    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        userImageImagview.setImageBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionUtility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                } else {
                    //code for deny
                }
                break;

            case PermissionUtility.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


}
