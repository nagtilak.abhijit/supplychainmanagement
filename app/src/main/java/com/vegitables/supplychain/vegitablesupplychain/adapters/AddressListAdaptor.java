package com.vegitables.supplychain.vegitablesupplychain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.models.ShippingAddress;

import java.util.List;

/**
 * Created by Abhi on 21-04-2019.
 */

public class AddressListAdaptor extends RecyclerView.Adapter<AddressListAdaptor.ViewHolder> {

    public OnItemClick listioner;
    List<ShippingAddress> shippingAddresses;
    private Context mContext;

    public AddressListAdaptor(Context mContext, List<ShippingAddress> shippingAddresses, OnItemClick listioner) {
        this.mContext = mContext;
        this.shippingAddresses = shippingAddresses;
        this.listioner = listioner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_address_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        ShippingAddress shippingAddress = shippingAddresses.get(position);
        viewHolder.tvAddressLine1.setText(shippingAddress.addressLine1);
        viewHolder.tvAddressLine2.setText(shippingAddress.addressLine2);
        viewHolder.tvAddressstate.setText(shippingAddress.village + " ," + shippingAddress.taluka);
        viewHolder.tvAddressdistrict.setText(shippingAddress.state + "," + shippingAddress.district);
        viewHolder.tvAddresspin.setText(shippingAddress.pincode);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.OnItemClicked(position);
            }
        });

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        int color1 = generator.getColor(shippingAddress.addressLine1);

        TextDrawable ic1 = TextDrawable.builder()
                .buildRound((shippingAddress.addressLine1.charAt(0) + "").toUpperCase() + "", color1);
        viewHolder.image_view.setImageDrawable(ic1);

    }

    @Override
    public int getItemCount() {
        return shippingAddresses.size();
    }

    public interface OnItemClick {
        public void OnItemClicked(int pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        private final TextView tvAddressLine1;
        private final TextView tvAddressLine2;
        private final TextView tvAddressstate;
        private final TextView tvAddressdistrict;
        private final TextView tvAddresspin;
        private final ImageView image_view;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAddressLine1 = (TextView) itemView.findViewById(R.id.tvAddressLine1);
            tvAddressLine2 = (TextView) itemView.findViewById(R.id.tvAddressLine2);
            tvAddressstate = (TextView) itemView.findViewById(R.id.tvAddressstate);
            tvAddressdistrict = (TextView) itemView.findViewById(R.id.tvAddressdistrict);
            tvAddresspin = (TextView) itemView.findViewById(R.id.tvAddresspin);
            image_view = (ImageView) itemView.findViewById(R.id.image_view);

        }
    }
}

