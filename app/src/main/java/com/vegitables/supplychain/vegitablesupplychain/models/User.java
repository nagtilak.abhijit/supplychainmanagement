package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Abhi on 21-04-2019.
 */

public class User implements Serializable {

    @SerializedName("accountNumber")
    @Expose
    public String accountNumber;
    @SerializedName("fullName")
    @Expose
    public String fullName;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("panNumber")
    @Expose
    public String panNumber;
    @SerializedName("ifscCode")
    @Expose
    public String ifscCode;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("shippingAddresses")
    @Expose
    public List<Object> shippingAddresses = null;
    @SerializedName("username")
    @Expose
    public String username;


}
