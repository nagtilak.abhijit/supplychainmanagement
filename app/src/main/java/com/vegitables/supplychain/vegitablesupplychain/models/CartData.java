package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 12-05-2019.
 */

public class CartData {
    @SerializedName("cart")
    @Expose
    public CartList cart;
}
