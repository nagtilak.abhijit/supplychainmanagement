package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 29-04-2019.
 */

public class CartItem {

    @SerializedName("sellOrderToken")
    @Expose
    public String sellOrderToken;
    @SerializedName("price")
    @Expose
    public String price;
}
