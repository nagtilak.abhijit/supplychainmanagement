package com.vegitables.supplychain.vegitablesupplychain.retrofit;

import android.util.Log;

import com.vegitables.supplychain.vegitablesupplychain.utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Abhi on 18-04-2019.
 */

public class RetrofitClient {

    public static Retrofit retrofit;

    public static Retrofit getRetrofit() {

        if (retrofit == null) {

            final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    Request request = original.newBuilder()
                            .addHeader("Accept", "application/json")
                            .addHeader("Content-Type", "application/json")
                            .build();

                    Response response =  chain.proceed(request);
                    Log.d("MyApp", "Code : "+response.code());
                    if (response.code() == 401){
                        // Magic is here ( Handle the error as your way )
                        return response;
                    }

                    return response;
                }
            });

            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();




        }

        return retrofit;
    }

    public void RetrofitClient() {

    }
}

