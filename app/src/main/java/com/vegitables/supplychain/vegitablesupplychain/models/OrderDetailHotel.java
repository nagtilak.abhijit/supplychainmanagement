package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Abhi on 14-05-2019.
 */

public class OrderDetailHotel implements Serializable {
    @SerializedName("cart")
    @Expose
    public CartList cart;
    @SerializedName("createdOn")
    @Expose
    public String createdOn;
    @SerializedName("farmerDetails")
    @Expose
    private FarmerDetails farmerDetails;
    @SerializedName("isShipped")
    @Expose
    public Boolean isShipped;
    @SerializedName("purchase_order_token")
    @Expose
    public String purchaseOrderToken;
    @SerializedName("shippingAddress")
    @Expose
    public ShippingAddress shippingAddress;
    @SerializedName("totalPrice")
    @Expose
    public String totalPrice;
}
