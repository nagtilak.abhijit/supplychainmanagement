package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 21-04-2019.
 */

public  class ResponseData {
    @SerializedName("isLoggedIn")
    @Expose
    public Boolean isLoggedIn;
    @SerializedName("loggedInTime")
    @Expose
    public String loggedInTime;
    @SerializedName("loggedOutTime")
    @Expose
    public String loggedOutTime;
    @SerializedName("login_token")
    @Expose
    public String loginToken;
    @SerializedName("cartItems")
    @Expose
    public String cartItems;
    @SerializedName("user")
    @Expose
    public User user;
    @SerializedName("userType")
    @Expose
    public String userType;
    @SerializedName("hotelName")
    @Expose
    public String hotelName;
    @SerializedName("gstnNumber")
    @Expose
    public String gstnNumber;
    @SerializedName("firmName")
    @Expose
    public String firmName;
}
