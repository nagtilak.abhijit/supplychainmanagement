package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 24-04-2019.
 */

public class AddressResponce {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("responseData")
    @Expose
    public ShippingAddressResponce responseData;
    @SerializedName("status")
    @Expose
    public Boolean status;
}
