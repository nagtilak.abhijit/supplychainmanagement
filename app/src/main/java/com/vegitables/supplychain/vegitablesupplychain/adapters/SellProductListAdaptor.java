package com.vegitables.supplychain.vegitablesupplychain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.models.SellProduct;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;

import java.util.List;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PROFILE_BASE_URL;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERTYPE;

/**
 * Created by Abhi on 21-04-2019.
 */

public class SellProductListAdaptor extends RecyclerView.Adapter<SellProductListAdaptor.ViewHolder> {

    private final String userType;
    List<SellProduct> products;
    CartAdd listioner;
    private Context mContext;

    public SellProductListAdaptor(Context mContext, List<SellProduct> products, CartAdd listioner) {
        this.mContext = mContext;
        this.products = products;
        this.listioner = listioner;
        userType = SharedPreferencesManager.getSomeStringValue(mContext, USERTYPE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.sellproduct_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        SellProduct sellproduct = products.get(position);
        RequestOptions options = new RequestOptions();
        options.error(R.drawable.no_image).placeholder(R.drawable.no_image).diskCacheStrategy(DiskCacheStrategy.ALL);
        String imgpath = PROFILE_BASE_URL + sellproduct.productImage;

        Glide.with(mContext)
                .load(imgpath)
                .apply(options)
                .into(viewHolder.productImg);

        viewHolder.productName.setText(sellproduct.product.productName);
        if (sellproduct.quality.equalsIgnoreCase("1"))
            viewHolder.categoryName.setText("Good");
        else if (sellproduct.quality.equalsIgnoreCase("2"))
            viewHolder.categoryName.setText("Better");
        else if (sellproduct.quality.equalsIgnoreCase("3"))
            viewHolder.categoryName.setText("Best");
        viewHolder.brandName.setText(sellproduct.product.brandName);
        viewHolder.productPrice.setText(sellproduct.price + " " + mContext.getString(R.string.Rs));
        viewHolder.productFeature.setText(sellproduct.product.features);
        viewHolder.btnSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.addToCart(position);
            }
        });
        if (sellproduct.isInCart) {
            viewHolder.btnSell.setVisibility(View.GONE);
        }

        if (sellproduct.orderStatus.equalsIgnoreCase("In Stock")) {
            if (sellproduct.isInCart)
                viewHolder.btnSell.setVisibility(View.GONE);
            else
                viewHolder.btnSell.setVisibility(View.VISIBLE);

        } else {

            viewHolder.btnSell.setVisibility(View.GONE);
            viewHolder.lld.setBackgroundResource(R.drawable.out_of_stock);
            viewHolder.lld.getBackground().setAlpha(120);
        }
    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    public interface CartAdd {

        public void addToCart(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName;
        public TextView categoryName;
        public TextView brandName;
        public TextView productFeature;
        public TextView productPrice;
        public EditText etQuantity;
        public Button btnSell;
        ImageView productImg;
        LinearLayout lld;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            productImg = (ImageView) itemView.findViewById(R.id.productImg);
            productImg = (ImageView) itemView.findViewById(R.id.productImg);
            productName = (TextView) itemView.findViewById(R.id.productName);
            categoryName = (TextView) itemView.findViewById(R.id.categoryName);
            brandName = (TextView) itemView.findViewById(R.id.brandName);
            productPrice = (TextView) itemView.findViewById(R.id.productPrice);
            productFeature = (TextView) itemView.findViewById(R.id.productFeature);
            etQuantity = (EditText) itemView.findViewById(R.id.etQuantity);
            btnSell = (Button) itemView.findViewById(R.id.btnAddCart);
            lld = (LinearLayout) itemView.findViewById(R.id.lld);

        }
    }

}

