package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Abhi on 24-04-2019.
 */

public class ShippingAddress implements Serializable {
    @SerializedName("addressId")
    @Expose
    public Integer addressId;
    @SerializedName("address_line1")
    @Expose
    public String addressLine1;
    @SerializedName("address_line2")
    @Expose
    public String addressLine2;
    @SerializedName("district")
    @Expose
    public String district;
    @SerializedName("pincode")
    @Expose
    public String pincode;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("taluka")
    @Expose
    public String taluka;
    @SerializedName("village")
    @Expose
    public String village;
}
