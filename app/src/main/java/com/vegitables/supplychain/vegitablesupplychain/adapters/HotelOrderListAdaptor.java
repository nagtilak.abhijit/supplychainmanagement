package com.vegitables.supplychain.vegitablesupplychain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.models.OrderDetailHotel;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERTYPE;

/**
 * Created by Abhi on 21-04-2019.
 */

public class HotelOrderListAdaptor extends RecyclerView.Adapter<HotelOrderListAdaptor.ViewHolder> {

    private final String userType;
    private Context mContext;
    private List<OrderDetailHotel> mOrderList;
    private IClick listioner;

    public HotelOrderListAdaptor(Context mContex, List<OrderDetailHotel> mOrderList, IClick listioner) {
        this.mContext = mContex;
        this.listioner = listioner;
        this.mOrderList = mOrderList;
        userType = SharedPreferencesManager.getSomeStringValue(mContext, USERTYPE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.hotel_order_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        OrderDetailHotel sellOrder = mOrderList.get(position);
        viewHolder.tvName.setText("" + sellOrder.cart.id);

        viewHolder.tvCatName.setText(" " + sellOrder.cart.totalPrice + " " + mContext.getString(R.string.Rs));
//        viewHolder.tvBrandName.setText(sellOrder.product.brandName);
        viewHolder.tvDate.setText(ConvertDate(sellOrder.createdOn));
        viewHolder.tvAddress.setText(sellOrder.shippingAddress.addressLine1 + "," + sellOrder.shippingAddress.addressLine2 + " " + sellOrder.shippingAddress.village + "," + sellOrder.shippingAddress.district + ", " + sellOrder.shippingAddress.taluka + ", " + sellOrder.shippingAddress.pincode);


        if (sellOrder.isShipped) {

            viewHolder.tvTotal.setTextColor(mContext.getResources().getColor(R.color.green));
            viewHolder.tvTotal.setText("ShipMent Status :" + "Success");

        } else {
            viewHolder.tvTotal.setTextColor(mContext.getResources().getColor(R.color.colorRed));
            viewHolder.tvTotal.setText("ShipMent Status :" + "Pending(Not Shipped)");

        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.OnItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOrderList.size();
    }

    public String ConvertDate(String dateString) {
        // convert seconds to milliseconds
        Date date = new Date(Long.parseLong(dateString));
// the format of your date
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
// give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT+5:30"));
        String formattedDate = sdf.format(date);
        return formattedDate;
    }


    public interface IClick {

        public void OnItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private final TextView tvName;
        private final TextView tvCatName;
        private final TextView tvDate;
        private final TextView tvTotal;
        private final TextView tvAddress;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCatName = (TextView) itemView.findViewById(R.id.tvCatName);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvTotal = (TextView) itemView.findViewById(R.id.tvTotal);
            tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);

        }
    }
}

