package com.vegitables.supplychain.vegitablesupplychain.services;

import com.vegitables.supplychain.vegitablesupplychain.interfaces.IAPICall;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.models.AddToCart;
import com.vegitables.supplychain.vegitablesupplychain.models.AddressResponce;
import com.vegitables.supplychain.vegitablesupplychain.models.Cart;
import com.vegitables.supplychain.vegitablesupplychain.models.FarmerOrdersResponce;
import com.vegitables.supplychain.vegitablesupplychain.models.HotelSellProduct;
import com.vegitables.supplychain.vegitablesupplychain.models.InDecQuan;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginRequest;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginResponse;
import com.vegitables.supplychain.vegitablesupplychain.models.NewAddress;
import com.vegitables.supplychain.vegitablesupplychain.models.PlaceOrder;
import com.vegitables.supplychain.vegitablesupplychain.models.ProductListResponce;
import com.vegitables.supplychain.vegitablesupplychain.models.PurchaseOrders;
import com.vegitables.supplychain.vegitablesupplychain.retrofit.RetrofitClient;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ADDRESSLIST;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ADD_CART;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.GET_CART;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.GET_PROFILE;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.LOGIN_ENDPOINT;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PLACE_ORDER;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PRODUCT_LIST;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.REGISTRATION;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.SELL_PRODUCT;

/**
 * Created by Abhi on 21-04-2019.
 */

public class APIService {
    private IAPICall authAPIs;
    private IApisCallbacks apisCallbacks;

    public APIService(IApisCallbacks apisCallbacks) {
        this.apisCallbacks = apisCallbacks;
        this.authAPIs = RetrofitClient.getRetrofit().create(IAPICall.class);
    }

    public void login(LoginRequest loginRequest) {

        Call<LoginResponse> call = authAPIs.login(loginRequest);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), LOGIN_ENDPOINT);
                    } else
                        apisCallbacks.onFailure("Invalid username/password");

                } else if (response.code() == 400)
                    apisCallbacks.onFailure("Something Went Wrong");
                else if (response.code() == 500)
                    apisCallbacks.onFailure("Something Went Wrong");
                else if (response.code() == 401)
                    apisCallbacks.onFailure("Something Went Wrong");
                else
                    apisCallbacks.onFailure(response.message());
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });

    }


    public void getProducts() {

        Call<ProductListResponce> call = authAPIs.getProducts();

        call.enqueue(new Callback<ProductListResponce>() {
            @Override
            public void onResponse(Call<ProductListResponce> call, Response<ProductListResponce> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), PRODUCT_LIST);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<ProductListResponce> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void getProducts(String userid) {

        Call<ProductListResponce> call = authAPIs.getProducts(userid);

        call.enqueue(new Callback<ProductListResponce>() {
            @Override
            public void onResponse(Call<ProductListResponce> call, Response<ProductListResponce> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), PRODUCT_LIST);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<ProductListResponce> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void farmerRegistration(MultipartBody.Part profile_pic, RequestBody userTypeReq, RequestBody userFullNameReq, RequestBody userNameReq, RequestBody userpasswordReq, RequestBody userphoneReq, RequestBody userPANReq, RequestBody userAccNoReq, RequestBody userifscReq, RequestBody userfirmReq) {

        Call<LoginResponse> call = authAPIs.farmerRegistration(profile_pic, userNameReq, userpasswordReq, userTypeReq, userFullNameReq, userphoneReq, userPANReq, userAccNoReq, userfirmReq, userifscReq);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), REGISTRATION);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void hotelRegistration(MultipartBody.Part profile_pic, RequestBody userTypeReq, RequestBody userFullNameReq, RequestBody userNameReq, RequestBody userpasswordReq, RequestBody userphoneReq, RequestBody userPANReq, RequestBody userAccNoReq, RequestBody userHotelNameReq, RequestBody gstEditText, RequestBody ifsc) {

        Call<LoginResponse> call = authAPIs.hotelRegistration(profile_pic, userNameReq, userpasswordReq, userTypeReq, userFullNameReq, userphoneReq, userPANReq, userAccNoReq, userHotelNameReq, gstEditText, ifsc);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), REGISTRATION);
                    } else
                        apisCallbacks.onFailure(response.body().message);

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                apisCallbacks.onFailure("Something Went Wrong");
            }
        });
    }

    public void getNewAddressList(String userId) {

        Call<AddressResponce> call = authAPIs.getAddressList(userId);

        call.enqueue(new Callback<AddressResponce>() {
            @Override
            public void onResponse(Call<AddressResponce> call, Response<AddressResponce> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), "ADDRESSLIST");
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<AddressResponce> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void addNewAddressList(NewAddress shippingAddress) {

        Call<com.vegitables.supplychain.vegitablesupplychain.models.Response> call = authAPIs.addNewAddress(shippingAddress);

        call.enqueue(new Callback<com.vegitables.supplychain.vegitablesupplychain.models.Response>() {
            @Override
            public void onResponse(Call<com.vegitables.supplychain.vegitablesupplychain.models.Response> call, Response<com.vegitables.supplychain.vegitablesupplychain.models.Response> response) {

                if (response.code() == 200 || response.code() == 201) {

                    apisCallbacks.onSuccess(response.body(), ADDRESSLIST);

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<com.vegitables.supplychain.vegitablesupplychain.models.Response> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void getUserData(String userId) {

        Call<LoginResponse> call = authAPIs.getUserProfile(userId);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), GET_PROFILE);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void getCartItems(String userId) {

        Call<Cart> call = authAPIs.getCartItems(userId);

        call.enqueue(new Callback<Cart>() {
            @Override
            public void onResponse(Call<Cart> call, Response<Cart> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), GET_CART);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else if (response.code() == 404)
                    apisCallbacks.onSuccess(response.body(), GET_CART);
            }

            @Override
            public void onFailure(Call<Cart> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void sellProduct(MultipartBody.Part profile_pic, RequestBody productIdReq, RequestBody userIdReq, RequestBody productQuanReq, RequestBody productTotalPriceReq, RequestBody productQualityReq) {
        Call<ProductListResponce> call = authAPIs.sellProduct(profile_pic, userIdReq, productIdReq, productQuanReq, productTotalPriceReq,productQualityReq);

        call.enqueue(new Callback<ProductListResponce>() {
            @Override
            public void onResponse(Call<ProductListResponce> call, Response<ProductListResponce> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), SELL_PRODUCT);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<ProductListResponce> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void getFarmerOrders(String userId) {
        Call<FarmerOrdersResponce> call = authAPIs.farmerOrders(userId);

        call.enqueue(new Callback<FarmerOrdersResponce>() {
            @Override
            public void onResponse(Call<FarmerOrdersResponce> call, Response<FarmerOrdersResponce> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), SELL_PRODUCT);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<FarmerOrdersResponce> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }


    public void getHotelSellProduct(String userId) {
        Call<HotelSellProduct> call = authAPIs.HotelSellProduct(userId);

        call.enqueue(new Callback<HotelSellProduct>() {
            @Override
            public void onResponse(Call<HotelSellProduct> call, Response<HotelSellProduct> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), SELL_PRODUCT);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<HotelSellProduct> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }


    public void farmerUpdateProfile(String userId, MultipartBody.Part profile_pic, RequestBody userFullNameReq, RequestBody userphoneReq, RequestBody userPANReq, RequestBody userAccNoReq, RequestBody userfirmReq, RequestBody userifscReq) {

        Call<LoginResponse> call = authAPIs.farmerUpdate(userId, profile_pic, userFullNameReq, userphoneReq, userPANReq, userAccNoReq,userfirmReq,userifscReq);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), "Farmer Update");
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void hotelUpdateProfile(String userId, MultipartBody.Part profile_pic, RequestBody userFullNameReq, RequestBody userphoneReq, RequestBody userPANReq, RequestBody userAccNoReq, RequestBody userHotelNameReq, RequestBody usergstReq,RequestBody userIfscReq) {

        Call<LoginResponse> call = authAPIs.hotelUpdate(userId, profile_pic, userFullNameReq, userphoneReq, userPANReq, userAccNoReq, userHotelNameReq, usergstReq,userIfscReq);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), "Hotel Update");
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void addTocart(AddToCart addToCart) {

        Call<LoginResponse> call = authAPIs.addCart(addToCart);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), ADD_CART);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void ChangeQuantity(String userId, InDecQuan addToCart) {

        Call<Cart> call = authAPIs.ChangeQuantity(userId, addToCart);

        call.enqueue(new Callback<Cart>() {
            @Override
            public void onResponse(Call<Cart> call, Response<Cart> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), "CHANGE_QUANTITY");
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<Cart> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void placeOrderAPI(PlaceOrder placeOrder) {

        Call<LoginResponse> call = authAPIs.PlaceOrder(placeOrder);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), PLACE_ORDER);
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }

    public void getHotelOrders(String s) {
        Call<PurchaseOrders> call = authAPIs.hotelorder(s);

        call.enqueue(new Callback<PurchaseOrders>() {
            @Override
            public void onResponse(Call<PurchaseOrders> call, Response<PurchaseOrders> response) {

                if (response.code() == 200 || response.code() == 201) {

                    if (response.body().status) {
                        apisCallbacks.onSuccess(response.body(), "Hotel_order");
                    } else
                        apisCallbacks.onFailure("Something Went Wrong");

                } else
                    apisCallbacks.onFailure("Something Went Wrong");
            }

            @Override
            public void onFailure(Call<PurchaseOrders> call, Throwable t) {
                apisCallbacks.onFailure("No Internet Connection");
            }
        });
    }
}
