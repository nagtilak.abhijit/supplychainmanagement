package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 14-05-2019.
 */

public class PurchaseOrders {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("responseData")
    @Expose
    public OrderHotel responseData;
    @SerializedName("status")
    @Expose
    public Boolean status;
}
