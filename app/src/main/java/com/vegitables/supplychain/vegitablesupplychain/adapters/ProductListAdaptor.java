package com.vegitables.supplychain.vegitablesupplychain.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.models.Product;
import com.vegitables.supplychain.vegitablesupplychain.utils.FileUtils;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;

import java.io.File;
import java.util.List;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PROFILE_BASE_URL;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERTYPE;

/**
 * Created by Abhi on 21-04-2019.
 */

public class ProductListAdaptor extends RecyclerView.Adapter<ProductListAdaptor.ViewHolder> {

    private final String userType;
    List<Product> mProduvtList;
    OnQuantityChanged listioner;
    private Context mContext;

    public ProductListAdaptor(Context mContext, List<Product> mProduvtList, OnQuantityChanged listioner) {
        this.mContext = mContext;
        this.listioner = listioner;
        this.mProduvtList = mProduvtList;

        userType = SharedPreferencesManager.getSomeStringValue(mContext, USERTYPE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.product_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        if (userType.equalsIgnoreCase("farmer")) {
            viewHolder.llQuan.setVisibility(View.VISIBLE);
            viewHolder.llquality.setVisibility(View.VISIBLE);

        } else {
            viewHolder.btnSell.setVisibility(View.GONE);
            viewHolder.btnPickPicture.setVisibility(View.GONE);
            viewHolder.llprice.setVisibility(View.GONE);
            viewHolder.llQuan.setVisibility(View.GONE);
            viewHolder.llquality.setVisibility(View.GONE);
        }

        Product product = mProduvtList.get(position);
        RequestOptions options = new RequestOptions();
        options.error(R.drawable.no_image).placeholder(R.drawable.no_image).diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(mContext)
                .load(PROFILE_BASE_URL + product.productImage)
                .apply(options)
                .into(viewHolder.productImg);

        viewHolder.productName.setText(product.productName);
        viewHolder.categoryName.setText(product.categoryName);
        viewHolder.brandName.setText(product.brandName);
        viewHolder.productPrice.setText(product.price + " " + mContext.getString(R.string.Rs));
        viewHolder.productFeature.setText(product.features);
        viewHolder.etQuantity.setText(product.productQuantity);
        viewHolder.fprice.setText(product.fprice);

        viewHolder.etQuantity.setText(product.productQuantity + "");
        if (Integer.parseInt(product.productQuantity) >= 1) {
            if (Integer.parseInt(product.productQuantity) == 0) {
                viewHolder.btnSell.setEnabled(false);
            } else {
                viewHolder.btnSell.setEnabled(true);
            }
        } else {
            viewHolder.btnSell.setEnabled(false);
        }
        viewHolder.fprice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                listioner.productPriceChanged(viewHolder, position);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        viewHolder.plusImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.addQuantity(viewHolder, position);
            }
        });
        viewHolder.btnPickPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.productImagePicked(viewHolder, position);
            }
        });


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listioner.getProductTosell(viewHolder, position);
            }
        });

        viewHolder.rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                listioner.productQualityChose(viewHolder, position);
            }
        });


        viewHolder.minusImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.minusQuantity(viewHolder, position);
            }
        });
        viewHolder.btnSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.sellProduct(viewHolder, position);
            }
        });
        if (product.imagePtah.equalsIgnoreCase("")) {
            viewHolder.imgProduct.setVisibility(View.GONE);
        } else {
            viewHolder.imgProduct.setVisibility(View.VISIBLE);
            File file = new File(product.imagePtah);
            FileUtils.getRealPathFromURI(mContext, Uri.fromFile(file));
            Glide.with(mContext)
                    .load(file)
                    .apply(options)
                    .into(viewHolder.imgProduct);

        }
    }

    public void searchFilter(List<Product> productItemList) {
        this.mProduvtList = productItemList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mProduvtList.size();
    }


    public interface OnQuantityChanged {

        public void addQuantity(ViewHolder viewHolder, int position);

        public void minusQuantity(ViewHolder viewHolder, int position);

        public void sellProduct(ViewHolder viewHolder, int position);

        public void getProductTosell(ViewHolder viewHolder, int position);

        public void productPriceChanged(ViewHolder viewHolder, int position);

        public void productImagePicked(ViewHolder viewHolder, int position);

        public void productQualityChose(ViewHolder viewHolder, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName;
        public TextView categoryName;
        public TextView brandName;
        public TextView productFeature;
        public TextView productPrice;
        public EditText etQuantity;
        public ImageView plusImg;
        public ImageView minusImg;
        public Button btnSell;
        public Button btnPickPicture;
        public LinearLayout llQuan;
        public EditText fprice;
        public ImageView imgProduct;
        public RadioGroup rg;
        public RadioButton rb1;
        public RadioButton rb2;
        public RadioButton rb3;
        ImageView productImg;
        LinearLayout llprice;
        LinearLayout llquality;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            productImg = (ImageView) itemView.findViewById(R.id.productImg);
            plusImg = (ImageView) itemView.findViewById(R.id.plusImg);
            minusImg = (ImageView) itemView.findViewById(R.id.minusImg);
            productImg = (ImageView) itemView.findViewById(R.id.productImg);
            productName = (TextView) itemView.findViewById(R.id.productName);
            categoryName = (TextView) itemView.findViewById(R.id.categoryName);
            brandName = (TextView) itemView.findViewById(R.id.brandName);
            productPrice = (TextView) itemView.findViewById(R.id.productPrice);
            productFeature = (TextView) itemView.findViewById(R.id.productFeature);
            etQuantity = (EditText) itemView.findViewById(R.id.etQuantity);
            llprice = (LinearLayout) itemView.findViewById(R.id.llprice);
            llquality = (LinearLayout) itemView.findViewById(R.id.llquality);
            btnSell = (Button) itemView.findViewById(R.id.btnSell);
            btnPickPicture = (Button) itemView.findViewById(R.id.btnPickPicture);
            fprice = (EditText) itemView.findViewById(R.id.Price);
            llQuan = (LinearLayout) itemView.findViewById(R.id.llquan);
            imgProduct = (ImageView) itemView.findViewById(R.id.selectedImg);
            rg = (RadioGroup) itemView.findViewById(R.id.rgGender);
            rb1 = (RadioButton) itemView.findViewById(R.id.rbMale);
            rb2 = (RadioButton) itemView.findViewById(R.id.rbFemale);
            rb3 = (RadioButton) itemView.findViewById(R.id.rbOther);

        }
    }

}

