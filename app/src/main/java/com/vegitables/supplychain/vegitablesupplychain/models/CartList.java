package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhi on 12-05-2019.
  */

 public class CartList  implements Serializable {
    @SerializedName("cartItems")
    @Expose
    public List<CartProduct> cartItems = new ArrayList<>();
    @SerializedName("createdOn")
    @Expose
    public String createdOn;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("isActive")
    @Expose
    public Boolean isActive;
    @SerializedName("totalPrice")
    @Expose
    public String totalPrice;
 }
