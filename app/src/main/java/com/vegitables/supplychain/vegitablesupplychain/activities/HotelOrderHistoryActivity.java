package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.adapters.HotelOrderListAdaptor;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.CartProduct;
import com.vegitables.supplychain.vegitablesupplychain.models.OrderDetailHotel;
import com.vegitables.supplychain.vegitablesupplychain.models.PurchaseOrders;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.BASE_URL;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PLACE_ORDER;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERNAME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager.showLongToast;

/**
 * Created by Abhi on 21-04-2019.
 */

public class HotelOrderHistoryActivity extends BaseActivity implements IProgressBar, IApisCallbacks, HotelOrderListAdaptor.IClick {
    List<CartProduct> cartItems;
    private TextView mHeader;
    private ImageView mBackButton;
    private CustomProgressDialog progressDialog;
    private String userId;
    private RecyclerView mRvOders;
    private TextView noOrders;
    private HotelOrderListAdaptor orderListAdaptor;
    private List<OrderDetailHotel> mOrderList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_order);
        userId = SharedPreferencesManager.getSomeStringValue(this, USERNAME);
        initView();
        initListener();
        getOderData();
    }

    private void getOderData() {
        if (!NetworkManager.isNetworkAvailable(this)) {
            showLongToast(this, getString(R.string.no_internet));
            return;
        }
        showProgressBar();
        APIService service = new APIService(this);
        HttpUrl url = HttpUrl.parse(BASE_URL + PLACE_ORDER + "?userId=" + userId);
        service.getHotelOrders(url + "");
    }

    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        mBackButton = (ImageView) findViewById(R.id.backButton);
    }

    @Override
    protected void initListener() {
        mHeader.setText(getString(R.string.order_history));
        mRvOders = (RecyclerView) findViewById(R.id.rvOrders);
        noOrders = (TextView) findViewById(R.id.noOrders);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        initProgressBar();
    }


    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            ActivityCompat.finishAffinity(HotelOrderHistoryActivity.this);
        }
        startActivity(new Intent(HotelOrderHistoryActivity.this, MainActivity.class));
    }

    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        PurchaseOrders farmerOrdersResponce = (PurchaseOrders) response;
        if (farmerOrdersResponce.responseData.purchaseOrders.isEmpty()) {
            noOrders.setVisibility(View.VISIBLE);
            mRvOders.setVisibility(View.GONE);
        } else {
            noOrders.setVisibility(View.GONE);
            mRvOders.setVisibility(View.VISIBLE);
            mOrderList.clear();
            mOrderList.addAll(farmerOrdersResponce.responseData.purchaseOrders);
            SetAdaptor();

        }
    }

    private void SetAdaptor() {
        orderListAdaptor = new HotelOrderListAdaptor(this, mOrderList, this);
        mRvOders.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRvOders.setAdapter(orderListAdaptor);
    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        showLongToast(this, errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }


    @Override
    public void OnItemClick(int position) {
        Intent intent = new Intent(this, HotelOrderDetailActivity.class);
        intent.putExtra("list", (Serializable) mOrderList.get(position).cart.cartItems);
        startActivity(intent);

    }
}
