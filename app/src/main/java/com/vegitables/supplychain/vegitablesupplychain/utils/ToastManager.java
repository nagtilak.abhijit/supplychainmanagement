package com.vegitables.supplychain.vegitablesupplychain.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Abhi on 21-04-2019.
 */

public class ToastManager {
    public static void showLongToast(Context context, String message) {
        if (context != null)
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}
