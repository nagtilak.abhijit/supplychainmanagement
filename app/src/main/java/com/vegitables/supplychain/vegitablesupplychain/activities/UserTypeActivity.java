package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;

/**
 * Created by Abhi on 21-04-2019.
 */

public class UserTypeActivity extends BaseActivity {
    private TextView mHeader;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type_registration);
        initView();
        initListener();

    }

    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        mBackButton = (ImageView) findViewById(R.id.backButton);


    }

    @Override
    protected void initListener() {
        mHeader.setText(getString(R.string.register_as_text));
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void farmerRegistration(View view) {
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.putExtra("userType", "Farmer");
        startActivity(intent);
    }

    public void hotelRegistration(View view) {
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.putExtra("userType", "Hotel");
        startActivity(intent);
    }
}
