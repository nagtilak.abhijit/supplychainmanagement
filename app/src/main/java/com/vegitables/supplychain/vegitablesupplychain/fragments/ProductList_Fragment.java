package com.vegitables.supplychain.vegitablesupplychain.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.activities.FarmerOrderHistoryActivity;
import com.vegitables.supplychain.vegitablesupplychain.activities.MainActivity;
import com.vegitables.supplychain.vegitablesupplychain.adapters.ProductListAdaptor;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.Product;
import com.vegitables.supplychain.vegitablesupplychain.models.ProductListResponce;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.FileUtils;
import com.vegitables.supplychain.vegitablesupplychain.utils.KeyboardManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.PermissionUtility;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ISLOGGEDIN;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PRODUCT_LIST;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.SELL_PRODUCT;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERNAME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERTYPE;
import static com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager.showLongToast;

/**
 * Created by Abhi on 21-04-2019.
 */

public class ProductList_Fragment extends BaseFragment implements IApisCallbacks, IProgressBar, ProductListAdaptor.OnQuantityChanged {
    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;
    List<Product> mProductList = new ArrayList<>();
    private View mView;
    private RecyclerView mRvProduct;
    private ProductListAdaptor productListAdaptor;
    private CustomProgressDialog progressDialog;
    private EditText mEdtSearch;
    private String userType;
    private String userId;
    private TextView noProduct;
    private String productQuantity = "";
    private String productId = "";
    private Bitmap bitmap = null;
    private String productPrice;
    private boolean isLoggedIn;
    private int addPosition;
    private File file;
    private ArrayList<Image> selectedImages = new ArrayList<>();


    public ProductList_Fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_product_list, container, false);
        init(mView);
        initListeners();
        userId = SharedPreferencesManager.getSomeStringValue(getActivity(), USERNAME);
        userType = SharedPreferencesManager.getSomeStringValue(getActivity(), USERTYPE);
        isLoggedIn = SharedPreferencesManager.getSomeBooleanValue(getActivity(), ISLOGGEDIN);
        getProductData();
        return mView;


    }


    @Override
    public void init(View view) {

        mRvProduct = (RecyclerView) view.findViewById(R.id.rvProducts);
        mEdtSearch = (EditText) view.findViewById(R.id.edtSearch);
        noProduct = (TextView) view.findViewById(R.id.noProduct);

    }

    private void getProductData() {
        if (NetworkManager.isNetworkAvailable(getActivity())) {
            showProgressBar();
            KeyboardManager.hideKeyboard(getActivity());
            APIService service = new APIService(this);
            service.getProducts();

        } else {
            ToastManager.showLongToast(getActivity(), "No Internet Connection");
        }
    }

    private void setAdaptor() {
        productListAdaptor = new ProductListAdaptor(getActivity(), mProductList, this);
        mRvProduct.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRvProduct.setAdapter(productListAdaptor);
    }

    @Override
    public void initListeners() {
        mEdtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String input = editable.toString();
                filterListAndUpdate(input);
            }
        });
        initProgressBar();
    }


    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        if (requestTag.equalsIgnoreCase(PRODUCT_LIST)) {
            mProductList.clear();
            ProductListResponce productListResponce = (ProductListResponce) response;
            if (productListResponce.responseData.products.isEmpty()) {
                noProduct.setVisibility(View.VISIBLE);
                mRvProduct.setVisibility(View.GONE);
            } else {

                mProductList.addAll(productListResponce.responseData.products);
                setAdaptor();
                noProduct.setVisibility(View.GONE);
                mRvProduct.setVisibility(View.VISIBLE);
            }
        } else if (requestTag.equalsIgnoreCase(SELL_PRODUCT)) {
            ProductListResponce productListResponce = (ProductListResponce) response;
            if (productListResponce.status) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog_sell_completed);
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                Window window = dialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                Button btnOrder = (Button) dialog.findViewById(R.id.btn_ord);
                Button btnHome = (Button) dialog.findViewById(R.id.btnHome);
                btnHome.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        ((MainActivity) getActivity()).replaceFragment(new ProductList_Fragment());
                    }
                });
                btnOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), FarmerOrderHistoryActivity.class));
                    }
                });
                dialog.show();
            }
        }

    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        ToastManager.showLongToast(getActivity(), errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void filterListAndUpdate(String input) {

        List<Product> list = new ArrayList<>();

        if (TextUtils.isEmpty(input)) {
            list.addAll(mProductList);
        } else {
            for (int i = 0; i < mProductList.size(); i++) {
                if (mProductList.get(i).productName.toLowerCase().contains(input.toLowerCase())) {
                    mProductList.get(i).productQuantity = "0";
                    mProductList.get(i).fprice = "0";
                    list.add(mProductList.get(i));
                }
            }
        }

        productListAdaptor.searchFilter(list);

    }

    @Override
    public void addQuantity(ProductListAdaptor.ViewHolder viewHolder, int position) {
        int quan = Integer.parseInt(mProductList.get(position).productQuantity) + 1;
        mProductList.get(position).productQuantity = String.valueOf(quan);
        viewHolder.etQuantity.setText(quan + "");
        viewHolder.btnSell.setEnabled(true);


    }

    @Override
    public void minusQuantity(ProductListAdaptor.ViewHolder viewHolder, int position) {
        if (Integer.parseInt(mProductList.get(position).productQuantity) >= 1) {
            int quan = Integer.parseInt(mProductList.get(position).productQuantity) - 1;
            mProductList.get(position).productQuantity = String.valueOf(quan);
            viewHolder.etQuantity.setText(quan + "");
            if (Integer.parseInt(mProductList.get(position).productQuantity) == 0) {
                viewHolder.btnSell.setEnabled(false);
            } else {
                viewHolder.btnSell.setEnabled(true);
            }
        } else {
            viewHolder.btnSell.setEnabled(false);
        }
    }

    @Override
    public void sellProduct(ProductListAdaptor.ViewHolder viewHolder, int position) {
        productId = mProductList.get(position).productId;
        productQuantity = mProductList.get(position).productQuantity;
        if (!TextUtils.isEmpty(mProductList.get(position).fprice)) {
            if (Float.parseFloat(mProductList.get(position).fprice) > 0) {
                //  selectImage();
                sellProductMethod(position);

                productPrice = mProductList.get(position).fprice;
            } else {
                ToastManager.showLongToast(getActivity(), "Product Price must be greater than 0");
            }
        } else {
            ToastManager.showLongToast(getActivity(), "Enter Product Price to proceed");
        }
    }

    @Override
    public void getProductTosell(ProductListAdaptor.ViewHolder viewHolder, int position) {
        if (userType.equalsIgnoreCase("hotel")) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mProductList.get(position).productId + "");
// set Fragmentclass Arguments
            SellProductList_Fragment fragment = new SellProductList_Fragment();
            fragment.setArguments(bundle);
            ((FragmentActivity) getActivity()).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(fragment.getClass().getName())
                    .commit();
        }
    }

    @Override
    public void productPriceChanged(ProductListAdaptor.ViewHolder viewHolder, int position) {

        mProductList.get(position).fprice = viewHolder.fprice.getText().toString().trim();
    }

    @Override
    public void productImagePicked(ProductListAdaptor.ViewHolder viewHolder, int position) {
        addPosition = position;
        selectImage();
    }

    @Override
    public void productQualityChose(ProductListAdaptor.ViewHolder viewHolder, int position) {
        int selectedId = viewHolder.rg.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) viewHolder.rg.findViewById(selectedId);

        mProductList.get(position).quality = radioButton.getTag().toString();


    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose product Photo to Proceed");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    boolean result = PermissionUtility.checkCameraPermission(getActivity());
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    boolean result = PermissionUtility.checkStoragePermission(getActivity());
                    if (result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    // camera intent
    private void cameraIntent() {
        selectedImages.clear();
        ImagePicker.with(this)                         //  Initialize ImagePicker with activity or fragment context
                .setToolbarColor("#212121")         //  Toolbar color
                .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
                .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                .setProgressBarColor("#4CAF50")     //  ProgressBar color
                .setBackgroundColor("#212121")      //  Background color
                .setCameraOnly(true)               //  Camera mode
                .setSavePath("ImagePicker")          //  Selected images
                .start();

    }

    // gallery intent
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_FILE && resultCode == RESULT_OK)
            onSelectFromGalleryResult(data);
        else if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            selectedImages = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            mProductList.get(addPosition).imagePtah = selectedImages.get(0).getPath();
            setImageToImageView();

        }
        super.onActivityResult(requestCode, resultCode, data);


    }

    // select image from gallery
    private void onSelectFromGalleryResult(Intent data) {

        Uri imageUri = data.getData();
        mProductList.get(addPosition).imagePtah = FileUtils.getPath(getActivity().getApplicationContext(), imageUri);
        try {
            // bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
            setImageToImageView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sellProductMethod(int position) {
        if (!NetworkManager.isNetworkAvailable(getActivity())) {
            showLongToast(getActivity(), getString(R.string.no_internet));
            return;
        }
        if (mProductList.get(position).imagePtah.equalsIgnoreCase("")) {
            ToastManager.showLongToast(getActivity(), "Please select product Image");
            return;
        } else if (mProductList.get(position).quality.equalsIgnoreCase("")) {
            ToastManager.showLongToast(getActivity(), "Please select product quality");
            return;

        } else {
            File image = new File(mProductList.get(position).imagePtah);
            MultipartBody.Part profile_pic = null;
            showProgressBar();


            RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), image);
            profile_pic = MultipartBody.Part.createFormData("file", image.getName(), reqFile1);

            RequestBody productIdReq =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), productId);

            RequestBody userIdReq =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), userId);
            RequestBody productQuanReq =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), productQuantity + "");

            RequestBody productTotalPriceReq =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), productPrice + "");
            RequestBody productqualityReq =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), mProductList.get(position).quality + "");

            APIService service = new APIService(this);
            service.sellProduct(profile_pic, productIdReq, userIdReq, productQuanReq, productTotalPriceReq, productQuanReq);

        }

    }

    // capture image from camra
    private void onCaptureImageResult(Intent data) {
        //
        //  bitmap = (Bitmap) data.getExtras().get("data");
    }

    private void setImageToImageView() {
        productListAdaptor.searchFilter(mProductList);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionUtility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                } else {
                    //code for deny
                }
                break;

            case PermissionUtility.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }
}
