package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Abhi on 14-05-2019.
 */

public class PlaceOrder implements Serializable {
    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("totalPrice")
    @Expose
    public String totalPrice;
    @SerializedName("addressId")
    @Expose
    public Integer addressId;
}
