package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.adapters.AddressListAdaptor;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.AddressResponce;
import com.vegitables.supplychain.vegitablesupplychain.models.PlaceOrder;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import java.io.Serializable;

import okhttp3.HttpUrl;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ADDRESSLIST;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.BASE_URL;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERNAME;

/**
 * Created by Abhi on 24-04-2019.
 */

public class ActivityAddress extends BaseActivity implements IProgressBar, IApisCallbacks, AddressListAdaptor.OnItemClick {
    private TextView mHeader;
    private ImageView mBackButton;
    private TextView tvAddNewAddress;
    private RecyclerView rvAddress;
    private Dialog progressDialog;
    private String userId;
    private TextView tvNoAddress;
    private String value = "";
    private AddressResponce addressResponce;
    private String totalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        initView();
        initListener();
        if (getIntent().hasExtra("value"))
            value = getIntent().getStringExtra("value");
        if (getIntent().hasExtra("price"))
            totalPrice = getIntent().getStringExtra("price");
        userId = SharedPreferencesManager.getSomeStringValue(this, USERNAME);
        getAddress();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 1000) {
            getAddress();
        }
    }

    private void getAddress() {
        if (NetworkManager.isNetworkAvailable(this)) {
            showProgressBar();
            APIService service = new APIService(this);
            HttpUrl url = HttpUrl.parse(BASE_URL + ADDRESSLIST + "?userId=" + userId);

            service.getNewAddressList(url + "");
        } else {
            ToastManager.showLongToast(this, "No Internet Connection");
        }
    }

    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        mBackButton = (ImageView) findViewById(R.id.backButton);
        rvAddress = (RecyclerView) findViewById(R.id.rvAddress);
        tvAddNewAddress = (TextView) findViewById(R.id.tvAddNewAddress);
        tvNoAddress = (TextView) findViewById(R.id.noAddress);

    }

    @Override
    protected void initListener() {
        mHeader.setText(getString(R.string.address));
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        initProgressBar();


    }

    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        if (requestTag.equalsIgnoreCase("ADDRESSLIST")) {
            addressResponce = (AddressResponce) response;
            if (addressResponce.responseData.shippingAddresses != null) {
                AddressListAdaptor addressListAdaptor = new AddressListAdaptor(this, addressResponce.responseData.shippingAddresses, this);
                rvAddress.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
//                    LinearLayoutManager.VERTICAL);
//            dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.gray_line_divider));
//            rvAddress.addItemDecoration(dividerItemDecoration);
                rvAddress.setAdapter(addressListAdaptor);
                rvAddress.setVisibility(View.VISIBLE);
                tvNoAddress.setVisibility(View.GONE);
            } else {
                rvAddress.setVisibility(View.GONE);
                tvNoAddress.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        ToastManager.showLongToast(this, errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public void addAddressMethod(View view) {
        Intent intent = new Intent(this, AddNewAddressActivity.class);
        startActivityForResult(intent, 1000);
    }

    @Override
    public void OnItemClicked(int pos) {
        if (!TextUtils.isEmpty(value)) {
//            if (NetworkManager.isNetworkAvailable(this)) {
//                showProgressBar();
            PlaceOrder placeOrder = new PlaceOrder();
            placeOrder.userId = userId;
            placeOrder.addressId = addressResponce.responseData.shippingAddresses.get(pos).addressId;
            placeOrder.totalPrice = totalPrice;
//                APIService service = new APIService(this);
//                service.placeOrderAPI(placeOrder);
//            } else {
//                ToastManager.showLongToast(this, "No Internet Connection");
//            }
            Intent intent = new Intent(this, PaymentMethod.class);
            intent.putExtra("data", (Serializable) placeOrder);
            startActivity(intent);
        }
    }
}
