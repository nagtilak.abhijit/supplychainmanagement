package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 28-04-2019.
 */

public class FarmerOrdersResponce {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("responseData")
    @Expose
    public OrderDetail responseData;
    @SerializedName("status")
    @Expose
    public Boolean status;


}
