package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginRequest;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginResponse;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.KeyboardManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ACCOUNTNO;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.FULLNAME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ISLOGGEDIN;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.ITEMCOUNT;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.LOGGEDINTIME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.LOGINTOKEN;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.LOGOUTTIME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.MOBILE;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PANNO;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PHOTO;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERNAME;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERTYPE;

/**
 * Created by Abhi on 21-04-2019.
 */

public class LoginActivity extends BaseActivity implements IApisCallbacks, IProgressBar {
    private TextView mHeader;
    private ImageView mBackButton;
    private EditText mEtUserpass;
    private EditText mEtUserName;
    private CustomProgressDialog progressDialog;

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        initListener();
    }

    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        mBackButton = (ImageView) findViewById(R.id.backButton);
        mEtUserName = (EditText) findViewById(R.id.usernameEditText);
        mEtUserpass = (EditText) findViewById(R.id.passwordEditText);
    }

    @Override
    protected void initListener() {
        mHeader.setText(getString(R.string.login_text));
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        initProgressBar();


    }

    public void forgotPasswordClickedMethod(View view) {
        Toast.makeText(this, "Work in Progress", Toast.LENGTH_SHORT).show();
    }

    public void loginClickMethod(View view) {
        String username = mEtUserName.getText().toString();
        String userpass = mEtUserpass.getText().toString();

        if (TextUtils.isEmpty(username)) {
            mEtUserName.setError("Required");
            mEtUserName.setFocusable(true);
        } else if (TextUtils.isEmpty(userpass)) {
            mEtUserpass.setError("Required");
            mEtUserpass.setFocusable(true);
        } else if (!isValidEmail(username)) {
            ToastManager.showLongToast(this, "Enter Valid Email address");
        } else {
            if (NetworkManager.isNetworkAvailable(LoginActivity.this)) {
                showProgressBar();
                KeyboardManager.hideKeyboard(LoginActivity.this);
                LoginRequest loginRequest = new LoginRequest();
                loginRequest.username = username;
                loginRequest.password = userpass;

                APIService service = new APIService(this);
                service.login(loginRequest);
            } else {
                ToastManager.showLongToast(this, "No Internet Connection");
            }

        }
    }

    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        LoginResponse loginResponse = (LoginResponse) response;
        SharedPreferencesManager.setSomeBooleanValue(this, ISLOGGEDIN, loginResponse.responseData.isLoggedIn);
        SharedPreferencesManager.setSomeStringValue(this, LOGGEDINTIME, loginResponse.responseData.loggedInTime);
        SharedPreferencesManager.setSomeStringValue(this, LOGOUTTIME, loginResponse.responseData.loggedOutTime);
        SharedPreferencesManager.setSomeStringValue(this, LOGINTOKEN, loginResponse.responseData.loginToken);
        SharedPreferencesManager.setSomeStringValue(this, ACCOUNTNO, loginResponse.responseData.user.accountNumber);
        SharedPreferencesManager.setSomeStringValue(this, FULLNAME, loginResponse.responseData.user.fullName);
        SharedPreferencesManager.setSomeStringValue(this, MOBILE, loginResponse.responseData.user.username);
        SharedPreferencesManager.setSomeStringValue(this, PANNO, loginResponse.responseData.user.panNumber);
        SharedPreferencesManager.setSomeStringValue(this, PHOTO, loginResponse.responseData.user.photo);
        SharedPreferencesManager.setSomeStringValue(this, USERNAME, loginResponse.responseData.user.username);
        SharedPreferencesManager.setSomeStringValue(this, USERTYPE, loginResponse.responseData.userType);
        SharedPreferencesManager.setSomeStringValue(this, ITEMCOUNT, loginResponse.responseData.cartItems);
        gotoHome();


    }

    private void gotoHome() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            ActivityCompat.finishAffinity(this);
        }
        startActivity(new Intent(this, MainActivity.class));


    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        ToastManager.showLongToast(this, errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public void registerOnClickMethod(View view) {
        startActivity(new Intent(this, UserTypeActivity.class));

    }
}
