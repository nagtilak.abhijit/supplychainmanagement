package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 13-05-2019.
 */

public class NewAddress {
    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("address")
    @Expose
    public AddAddress address;
}
