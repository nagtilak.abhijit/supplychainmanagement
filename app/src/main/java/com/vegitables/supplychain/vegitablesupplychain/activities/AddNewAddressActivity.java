package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.AddAddress;
import com.vegitables.supplychain.vegitablesupplychain.models.NewAddress;
import com.vegitables.supplychain.vegitablesupplychain.models.Response;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.KeyboardManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERNAME;

/**
 * Created by Abhi on 21-04-2019.
 */

public class AddNewAddressActivity extends BaseActivity implements IProgressBar, IApisCallbacks {
    private TextView mHeader;
    private ImageView mBackButton;
    private EditText etaddress_line1;
    private EditText etaddress_line2;
    private EditText etaddress_pincode;
    private EditText etaddress_taluka;
    private EditText etaddress_district;
    private EditText etaddress_state;
    private Dialog progressDialog;
    private EditText etaddress_village;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);
        userId = SharedPreferencesManager.getSomeStringValue(this, USERNAME);

        initView();
        initListener();

    }

    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        mBackButton = (ImageView) findViewById(R.id.backButton);
        etaddress_line1 = (EditText) findViewById(R.id.etaddress_line1);
        etaddress_line2 = (EditText) findViewById(R.id.etaddress_line2);
        etaddress_state = (EditText) findViewById(R.id.etaddress_state);
        etaddress_district = (EditText) findViewById(R.id.etaddress_district);
        etaddress_taluka = (EditText) findViewById(R.id.etaddress_taluka);
        etaddress_village = (EditText) findViewById(R.id.etaddress_village);
        etaddress_pincode = (EditText) findViewById(R.id.etaddress_pincode);
    }

    @Override
    protected void initListener() {
        mHeader.setText(getString(R.string.add_new_address));
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        initProgressBar();
    }


    public void addAddressClickMethod(View view) {
        KeyboardManager.hideKeyboard(this);
        if (TextUtils.isEmpty(etaddress_line1.getText().toString().trim())) {
            etaddress_line1.setError(getString(R.string.required));
            etaddress_line1.setFocusable(true);

        } else if (TextUtils.isEmpty(etaddress_line2.getText().toString().trim())) {
            etaddress_line2.setError(getString(R.string.required));
            etaddress_line2.setFocusable(true);

        } else if (TextUtils.isEmpty(etaddress_state.getText().toString().trim())) {
            etaddress_state.setError(getString(R.string.required));
            etaddress_state.setFocusable(true);
        } else if (TextUtils.isEmpty(etaddress_district.getText().toString().trim())) {
            etaddress_district.setError(getString(R.string.required));
            etaddress_district.setFocusable(true);
        } else if (TextUtils.isEmpty(etaddress_taluka.getText().toString().trim())) {
            etaddress_taluka.setError(getString(R.string.required));
            etaddress_taluka.setFocusable(true);
        } else if (TextUtils.isEmpty(etaddress_pincode.getText().toString().trim())) {
            etaddress_pincode.setError(getString(R.string.required));
            etaddress_pincode.setFocusable(true);
        } else if (TextUtils.isEmpty(etaddress_village.getText().toString().trim())) {
            etaddress_village.setError(getString(R.string.required));
            etaddress_village.setFocusable(true);
        } else if (etaddress_pincode.getText().toString().trim().length() != 6) {
            ToastManager.showLongToast(this, "Enter Valid Pincode");
            etaddress_pincode.setFocusable(true);
        } else {
            NewAddress newAddress = new NewAddress();
            newAddress.userId = userId;
            AddAddress shippingAddress = new AddAddress();
            shippingAddress.addressLine1 = etaddress_line1.getText().toString().trim();
            shippingAddress.addressLine2 = etaddress_line2.getText().toString().trim();
            shippingAddress.state = etaddress_state.getText().toString().trim();
            shippingAddress.district = etaddress_district.getText().toString().trim();
            shippingAddress.taluka = etaddress_taluka.getText().toString().trim();
            shippingAddress.village = etaddress_village.getText().toString().trim();
            shippingAddress.pincode = etaddress_pincode.getText().toString().trim();
            newAddress.address = shippingAddress;
            if (NetworkManager.isNetworkAvailable(this)) {
                showProgressBar();
                APIService service = new APIService(this);
                service.addNewAddressList(newAddress);
            } else {
                ToastManager.showLongToast(this, "No Internet Connection");
            }
        }
    }


    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        Response addressResponce = (Response) response;
        ToastManager.showLongToast(this, addressResponce.message);
        Intent intent = new Intent();
        intent.putExtra("MESSAGE", addressResponce.message);
        setResult(1000, intent);
        finish();

    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        ToastManager.showLongToast(this, errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

}
