package com.vegitables.supplychain.vegitablesupplychain.fragments;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by Abhi on 21-04-2019.
 */


public abstract class BaseFragment extends Fragment {

    public abstract void init(View view);

    public abstract void initListeners();


}

