package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.custom_views.CustomProgressDialog;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IApisCallbacks;
import com.vegitables.supplychain.vegitablesupplychain.interfaces.IProgressBar;
import com.vegitables.supplychain.vegitablesupplychain.models.LoginResponse;
import com.vegitables.supplychain.vegitablesupplychain.services.APIService;
import com.vegitables.supplychain.vegitablesupplychain.utils.KeyboardManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.NetworkManager;
import com.vegitables.supplychain.vegitablesupplychain.utils.PermissionUtility;
import com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.vegitables.supplychain.vegitablesupplychain.utils.ToastManager.showLongToast;

/**
 * Created by Abhi on 21-04-2019.
 */

public class RegistrationActivity extends BaseActivity implements IProgressBar, IApisCallbacks {
    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;
    private TextView mHeader;
    private Bitmap bitmap = null;
    private ImageView mBackButton;
    private String userType;
    private EditText fullnameEditText;
    private EditText usernameEditText;
    private EditText confirmpasswordEditText;
    private EditText phoneNoEditText;
    private EditText passwordEditText;
    private EditText panNoEditText;
    private EditText accountNoEditText;
    private EditText profilePhotoEditText;
    private CustomProgressDialog progressDialog;
    private EditText hotelNameEditText;
    private EditText gstEditText;
    private LinearLayout llhotelname;
    private LinearLayout llgst;
    private View view1;
    private ImageView userImageImagview;
    private EditText ifscEditText;
    private EditText firmEditText;
    private LinearLayout llfirm;
    private ImageView mEditImagview;

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        if (getIntent().hasExtra("userType"))
            userType = getIntent().getStringExtra("userType");
        initView();
        initListener();
    }

    @Override
    protected void initView() {
        userImageImagview = (ImageView) findViewById(R.id.userImageImagview);
        mEditImagview = (ImageView) findViewById(R.id.editImagview);

        mHeader = (TextView) findViewById(R.id.headerText);
        fullnameEditText = (EditText) findViewById(R.id.fullnameEditText);
        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        confirmpasswordEditText = (EditText) findViewById(R.id.confirmpasswordEditText);
        phoneNoEditText = (EditText) findViewById(R.id.phoneNoEditText);
        accountNoEditText = (EditText) findViewById(R.id.accountNoEditText);
        panNoEditText = (EditText) findViewById(R.id.panNoEditText);
        hotelNameEditText = (EditText) findViewById(R.id.hotelNameEditText);
        gstEditText = (EditText) findViewById(R.id.gstEditText);
        ifscEditText = (EditText) findViewById(R.id.ifscEditText);
        firmEditText = (EditText) findViewById(R.id.firmEditText);
        llhotelname = (LinearLayout) findViewById(R.id.llhotelname);
        llfirm = (LinearLayout) findViewById(R.id.llfirm);
        llgst = (LinearLayout) findViewById(R.id.llgst);
        view1 = (View) findViewById(R.id.view1);
        gstEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        panNoEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        ifscEditText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        mBackButton = (ImageView) findViewById(R.id.backButton);
    }

    @Override
    protected void initListener() {
        if (userType.equalsIgnoreCase("farmer")) {
            mHeader.setText(getString(R.string.farmer_registration));
            llgst.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            llhotelname.setVisibility(View.GONE);
        } else if (userType.equalsIgnoreCase("hotel")) {
            mHeader.setText(getString(R.string.hotel_registration));
            llgst.setVisibility(View.VISIBLE);
            llfirm.setVisibility(View.GONE);
            llhotelname.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
        } else {
            mHeader.setText(getString(R.string.register_as_text));
        }
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        userImageImagview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userImageImagviewClicked();


            }
        });
        mEditImagview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userImageImagviewClicked();


            }
        });
        initProgressBar();
    }

    private void userImageImagviewClicked() {
        selectImage();
    }

    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    boolean result = PermissionUtility.checkCameraPermission(RegistrationActivity.this);
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    boolean result = PermissionUtility.checkStoragePermission(RegistrationActivity.this);
                    if (result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    // camera intent
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    // gallery intent
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    public void createAccountClickMethod(View view) {
        if (userType.equalsIgnoreCase("farmer")) {
            if (TextUtils.isEmpty(fullnameEditText.getText().toString())) {
                fullnameEditText.setError(getString(R.string.required));
                fullnameEditText.setFocusable(true);

            } else if (TextUtils.isEmpty(usernameEditText.getText().toString())) {
                usernameEditText.setError(getString(R.string.required));
                usernameEditText.setFocusable(true);

            } else if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
                passwordEditText.setError(getString(R.string.required));
                passwordEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(confirmpasswordEditText.getText().toString())) {
                confirmpasswordEditText.setError(getString(R.string.required));
                confirmpasswordEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(phoneNoEditText.getText().toString())) {
                phoneNoEditText.setError(getString(R.string.required));
                phoneNoEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(firmEditText.getText().toString())) {
                firmEditText.setError(getString(R.string.required));
                firmEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(accountNoEditText.getText().toString())) {
                accountNoEditText.setError(getString(R.string.required));
                accountNoEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(ifscEditText.getText().toString())) {
                ifscEditText.setError(getString(R.string.required));
                ifscEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(panNoEditText.getText().toString())) {
                panNoEditText.setError(getString(R.string.required));
                panNoEditText.setFocusable(true);
            } else if (!passwordEditText.getText().toString().equalsIgnoreCase(confirmpasswordEditText.getText().toString())) {
                showLongToast(this, "Password and Confirm Password must be same");

            } else if (!isValidEmail(usernameEditText.getText().toString())) {
                showLongToast(this, "Enter Valid Email");
            } else {
                if (userType.equalsIgnoreCase("farmer")) {
                    KeyboardManager.hideKeyboard(this);
                    if (!NetworkManager.isNetworkAvailable(this)) {
                        showLongToast(this, getString(R.string.no_internet));
                        return;
                    }

                    MultipartBody.Part profile_pic = null;

                    if (bitmap != null) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
                        String currentTimeStamp = dateFormat.format(new Date());
                        File file = new File(getCacheDir(), currentTimeStamp + ".jpeg");
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                        byte[] bitmapdata = bos.toByteArray();

                        try {
                            file.createNewFile();
                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(bitmapdata);
                            fos.flush();
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), file);
                        profile_pic = MultipartBody.Part.createFormData("file", file.getName(), reqFile1);

                    } else {
                        ToastManager.showLongToast(this, "Please Select Profile Picture");
                        return;
                    }
                    showProgressBar();

                    RequestBody userTypeReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), userType);

                    RequestBody userFullNameReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), fullnameEditText.getText().toString());
                    RequestBody userNameReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), usernameEditText.getText().toString());
                    RequestBody userpasswordReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), passwordEditText.getText().toString());

                    RequestBody userphoneReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), phoneNoEditText.getText().toString());
                    RequestBody userPANReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), panNoEditText.getText().toString());
                    RequestBody userAccNoReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), accountNoEditText.getText().toString());

                    RequestBody userfirmReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), firmEditText.getText().toString());
                    RequestBody userifscReq =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"), ifscEditText.getText().toString());

                    APIService service = new APIService(this);
                    service.farmerRegistration(profile_pic, userTypeReq, userFullNameReq, userNameReq, userpasswordReq, userphoneReq, userPANReq, userAccNoReq, userifscReq, userfirmReq);

                }
            }
        } else {
            if (TextUtils.isEmpty(fullnameEditText.getText().toString())) {
                fullnameEditText.setError(getString(R.string.required));
                fullnameEditText.setFocusable(true);

            } else if (TextUtils.isEmpty(usernameEditText.getText().toString())) {
                usernameEditText.setError(getString(R.string.required));
                usernameEditText.setFocusable(true);

            } else if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
                passwordEditText.setError(getString(R.string.required));
                passwordEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(confirmpasswordEditText.getText().toString())) {
                confirmpasswordEditText.setError(getString(R.string.required));
                confirmpasswordEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(phoneNoEditText.getText().toString())) {
                phoneNoEditText.setError(getString(R.string.required));
                phoneNoEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(accountNoEditText.getText().toString())) {
                accountNoEditText.setError(getString(R.string.required));
                accountNoEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(ifscEditText.getText().toString())) {
                ifscEditText.setError(getString(R.string.required));
                ifscEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(panNoEditText.getText().toString())) {
                panNoEditText.setError(getString(R.string.required));
                panNoEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(hotelNameEditText.getText().toString())) {
                hotelNameEditText.setError(getString(R.string.required));
                hotelNameEditText.setFocusable(true);
            } else if (TextUtils.isEmpty(gstEditText.getText().toString())) {
                gstEditText.setError(getString(R.string.required));
                gstEditText.setFocusable(true);
            } else if (!passwordEditText.getText().toString().equalsIgnoreCase(confirmpasswordEditText.getText().toString())) {
                showLongToast(this, "Password and Confirm Password must be same");

            } else if (!isValidEmail(usernameEditText.getText().toString())) {
                showLongToast(this, "Enter Valid Email");
            } else {
                KeyboardManager.hideKeyboard(this);
                if (!NetworkManager.isNetworkAvailable(this)) {
                    showLongToast(this, getString(R.string.no_internet));
                    return;
                }

                MultipartBody.Part profile_pic = null;

                if (bitmap != null) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
                    String currentTimeStamp = dateFormat.format(new Date());
                    File file = new File(getCacheDir(), currentTimeStamp + ".jpeg");

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        fos.write(bitmapdata);
                        fos.flush();
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), file);
                    profile_pic = MultipartBody.Part.createFormData("file", file.getName(), reqFile1);

                } else {
                    ToastManager.showLongToast(this, "Please Select Profile Picture");
                    hideProgressBar();
                    return;
                }
                showProgressBar();

                RequestBody userTypeReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), userType);

                RequestBody userFullNameReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), fullnameEditText.getText().toString());
                RequestBody userNameReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), usernameEditText.getText().toString());
                RequestBody userpasswordReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), passwordEditText.getText().toString());

                RequestBody userphoneReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), phoneNoEditText.getText().toString());
                RequestBody userPANReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), panNoEditText.getText().toString());
                RequestBody userAccNoReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), accountNoEditText.getText().toString());

                RequestBody userHotelNameReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), hotelNameEditText.getText().toString());
                RequestBody usergstReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), gstEditText.getText().toString());
                RequestBody userifscReq =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), ifscEditText.getText().toString());

                APIService service = new APIService(this);
                service.hotelRegistration(profile_pic, userTypeReq, userFullNameReq, userNameReq, userpasswordReq, userphoneReq, userPANReq, userAccNoReq, userHotelNameReq, usergstReq, userifscReq);


            }
        }
    }

    @Override
    public void onSuccess(Object response, String requestTag) {
        hideProgressBar();
        LoginResponse loginResponse = (LoginResponse) response;
        showLongToast(this, loginResponse.message);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            ActivityCompat.finishAffinity(this);
        }
        startActivity(new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }

    @Override
    public void onFailure(String errorMessage) {
        hideProgressBar();
        showLongToast(this, errorMessage);
    }

    @Override
    public void initProgressBar() {
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void showProgressBar() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);

        }
    }

    // select image from gallery
    private void onSelectFromGalleryResult(Intent data) {

        Uri imageUri = data.getData();

        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            userImageImagview.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // capture image from camra
    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        userImageImagview.setImageBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionUtility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    galleryIntent();
                } else {
                    //code for deny
                }
                break;

            case PermissionUtility.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }


}
