package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Abhi on 28-04-2019.
 */

public class OrderDetail {
    @SerializedName("SellOrders")
    @Expose
    public List<SellOrder> sellOrders = null;
}
