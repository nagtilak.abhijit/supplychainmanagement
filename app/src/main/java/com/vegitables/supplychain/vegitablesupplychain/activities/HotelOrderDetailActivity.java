package com.vegitables.supplychain.vegitablesupplychain.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.adapters.OrderDetailListAdaptor;
import com.vegitables.supplychain.vegitablesupplychain.models.CartProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhi on 21-04-2019.
 */

public class HotelOrderDetailActivity extends BaseActivity {
    List<CartProduct> cartItems = new ArrayList<>();
    private TextView mHeader;
    private ImageView mBackButton;
    private RecyclerView mRvOders;
    private TextView noOrders;
    private OrderDetailListAdaptor orderListAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_order_detail);
        if (getIntent().hasExtra("list"))
            cartItems = (List<CartProduct>) getIntent().getSerializableExtra("list");

        initView();
        initListener();
        SetAdaptor();
    }


    @Override
    protected void initView() {
        mHeader = (TextView) findViewById(R.id.headerText);
        mBackButton = (ImageView) findViewById(R.id.backButton);
    }

    @Override
    protected void initListener() {
        mHeader.setText(getString(R.string.order_history));
        mRvOders = (RecyclerView) findViewById(R.id.rvOrders);
        noOrders = (TextView) findViewById(R.id.noOrders);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    private void SetAdaptor() {
        orderListAdaptor = new OrderDetailListAdaptor(this, cartItems);
        mRvOders.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRvOders.setAdapter(orderListAdaptor);
    }


}
