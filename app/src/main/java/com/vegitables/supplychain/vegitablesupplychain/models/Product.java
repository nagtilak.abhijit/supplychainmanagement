package com.vegitables.supplychain.vegitablesupplychain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Abhi on 21-04-2019.
 */

public class Product implements Serializable {
    @SerializedName("brand")
    @Expose
    public String brandName;
    @SerializedName("category")
    @Expose
    public String categoryName;
    @SerializedName("features")
    @Expose
    public String features;
    @SerializedName("price")
    @Expose
    public Integer price;
    @SerializedName("productImage")
    @Expose
    public String productImage;
    @SerializedName("productName")
    @Expose
    public String productName;
    @SerializedName("productQuantity")
    @Expose
    public String productQuantity = "0";
    @SerializedName("productId")
    @Expose
    public String productId;

    @SerializedName("fprice")
    @Expose
    public String fprice = "";

    @SerializedName("imagePtah")
    @Expose
    public String imagePtah = "";

    @SerializedName("quality")
    @Expose
    public String quality = "";

}

