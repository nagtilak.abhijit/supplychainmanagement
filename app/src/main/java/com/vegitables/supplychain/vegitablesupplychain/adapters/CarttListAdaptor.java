package com.vegitables.supplychain.vegitablesupplychain.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vegitables.supplychain.vegitablesupplychain.R;
import com.vegitables.supplychain.vegitablesupplychain.models.CartProduct;
import com.vegitables.supplychain.vegitablesupplychain.utils.SharedPreferencesManager;

import java.util.List;

import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.PROFILE_BASE_URL;
import static com.vegitables.supplychain.vegitablesupplychain.utils.Constants.USERTYPE;

/**
 * Created by Abhi on 21-04-2019.
 */

public class CarttListAdaptor extends RecyclerView.Adapter<CarttListAdaptor.ViewHolder> {

    private final String userType;
    List<CartProduct> mProduvtList;
    OnQuantityChanged listioner;


    private Context mContext;

    public CarttListAdaptor(Context mContext, List<CartProduct> mProduvtList, OnQuantityChanged listioner) {
        this.mContext = mContext;
        this.mProduvtList = mProduvtList;
        this.listioner = listioner;

        userType = SharedPreferencesManager.getSomeStringValue(mContext, USERTYPE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cart_product_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        CartProduct product = mProduvtList.get(position);
        RequestOptions options = new RequestOptions();
        options.error(R.drawable.no_image).placeholder(R.drawable.no_image).diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(mContext)
                .load(PROFILE_BASE_URL + product.productDetails.productImage)
                .apply(options)
                .into(viewHolder.productImg);

        viewHolder.productName.setText(product.productDetails.product.productName);
        if (product.productDetails.quality.equalsIgnoreCase("1"))
            viewHolder.categoryName.setText("Good");
        else if (product.productDetails.quality.equalsIgnoreCase("2"))
            viewHolder.categoryName.setText("Better");
        else if (product.productDetails.quality.equalsIgnoreCase("3"))
            viewHolder.categoryName.setText("Best");
        viewHolder.brandName.setText(product.productDetails.product.brandName);
        viewHolder.productPrice.setText(product.price + " "+mContext.getString(R.string.Rs));
        viewHolder.productFeature.setText(product.productDetails.product.features);
        viewHolder.etQuantity.setText(product.quantity);

        viewHolder.plusImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.addQuantity(viewHolder, position);
            }
        });
        viewHolder.minusImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listioner.minusQuantity(viewHolder, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProduvtList.size();
    }

    public interface OnQuantityChanged {

        public void addQuantity(ViewHolder viewHolder, int position);

        public void minusQuantity(ViewHolder viewHolder, int position);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName;
        public TextView categoryName;
        public TextView brandName;
        public TextView productFeature;
        public TextView productPrice;
        public EditText etQuantity;
        public ImageView plusImg;
        public ImageView minusImg;
        public LinearLayout llQuan;
        ImageView productImg;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            productImg = (ImageView) itemView.findViewById(R.id.productImg);
            plusImg = (ImageView) itemView.findViewById(R.id.plusImg);
            minusImg = (ImageView) itemView.findViewById(R.id.minusImg);
            productImg = (ImageView) itemView.findViewById(R.id.productImg);
            productName = (TextView) itemView.findViewById(R.id.productName);
            categoryName = (TextView) itemView.findViewById(R.id.categoryName);
            brandName = (TextView) itemView.findViewById(R.id.brandName);
            productPrice = (TextView) itemView.findViewById(R.id.productPrice);
            productFeature = (TextView) itemView.findViewById(R.id.productFeature);
            etQuantity = (EditText) itemView.findViewById(R.id.etQuantity);
            llQuan = (LinearLayout) itemView.findViewById(R.id.llquan);

        }
    }

}

